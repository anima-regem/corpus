=അരുവിയൂര്‍കോട്ട=

പാണ്ഡ്യരാജ്യം ഭരിച്ചിരുന്ന മാറന്‍ ചടയന്‍ (765-815) ആക്രമിച്ച് നശിപ്പിച്ച കോട്ടകളില്‍ ഒന്ന്. ചടയന്റെ ഇരുപത്തിമൂന്നാം ഭരണവര്‍ഷത്തില്‍ മലനാട്ടുരാജാവിനെ തോല്പിച്ച്, അരുവിയൂര്‍കോട്ട നശിപ്പിച്ചതായി കലുങ്കുമലൈ ശാസനം (കഴുകുമല ശാസനം) പറയുന്നു. 'അരുവിയൂര്‍ക്കോട്ടൈ അഴുത്തു നന്റു ചെയ്ത്' എന്ന് ഈ സംഭവത്തെ പ്രസ്തുത ശാസനം പരാമര്‍ശിക്കുന്നു. അരുവിയൂര്‍ക്കോട്ട തിരുവട്ടാറിനടുത്തുള്ള അരുവിക്കരയിലാണ് എന്നു ചരിത്രകാരന്മാര്‍ കരുതുന്നു. അരുവിയൂര്‍കോട്ട ആയ്‍വംശക്കാരുടെ സുശക്തവും സുസജ്ജവുമായ സൈനികകേന്ദ്രമായിരുന്നു. 

(വി.ആര്‍. പരമേശ്വരന്‍ പിള്ള)