==കോസ്മസ്==
==Cosmas==

സഞ്ചാരസാഹിത്യകാരന്‍. അലക്സാണ്ട്രിയയില്‍ ജീവിച്ചിരുന്ന (6-ാം ശ.) ഇദ്ദേഹം 'ഇന്‍ഡികൊപ്ലീസ്റ്റൈസ്' (ഇന്ത്യാ യാത്രക്കാരന്‍) എന്നറിയപ്പെട്ടു. എ.ഡി. 530-നും 550-നും ഇടയ്ക്ക് എഴുതപ്പെട്ട ക്രിസ്ത്യന്‍ ടോപ്പോഗ്രഫി എന്ന ഗ്രന്ഥത്തിന്റെ കര്‍ത്താവെന്ന നിലയിലാണ് ഇദ്ദേഹം പരക്കെ അറിയപ്പെട്ടിരുന്നത്.

ചെങ്കടല്‍, പേര്‍ഷ്യന്‍ ഉള്‍ക്കടല്‍, ഇന്ത്യയുടെ പടിഞ്ഞാറന്‍തീരം എന്നിവിടങ്ങളില്‍ തുടര്‍ച്ചയായി യാത്ര ചെയ്തിരുന്ന ഇദ്ദേഹം സിലോണും എത്യോപ്യയും സന്ദര്‍ശിച്ചിരുന്നു. കോസ്മസിന്റെ യാത്രാവിവരണങ്ങളില്‍ ഇന്നവശേഷിക്കുന്നത് ക്രിസ്ത്യന്‍ ടോപ്പോഗ്രഫി എന്ന പേരില്‍ അറിയപ്പെടുന്ന 12 വാല്യങ്ങളിലുള്ള ഗ്രന്ഥം മാത്രമാണ്. ഈ ഗ്രന്ഥത്തില്‍ പ്രധാനപ്പെട്ട പല ഭൂപടങ്ങളും, ഇദ്ദേഹം സന്ദര്‍ശിച്ച പല സ്ഥലങ്ങളെക്കുറിച്ചുള്ള രസകരമായ വിവരണങ്ങളും ഉണ്ട്. കോസ്മസ് ആയിരുന്നു യൂറോപ്യന്മാരില്‍ ചൈനയെപ്പറ്റി ആദ്യമായി പരാമര്‍ശിച്ചിട്ടുള്ള എഴുത്തുകാരന്‍. ഇദ്ദേഹം ചൈനയെ 'സിനിസ്താ' എന്ന പേരിലാണു പരാമര്‍ശിച്ചിട്ടുള്ളത് (പേര്‍ഷ്യന്‍ ഭാഷയിലെ 'ചൈനിസ്താന്‍' എന്ന പദത്തില്‍ നിന്നായിരിക്കാം ഈ പദം രൂപംകൊണ്ടത്).

(ഡോ. എ.പി. ഇബ്രാഹിംകുഞ്ഞ്)