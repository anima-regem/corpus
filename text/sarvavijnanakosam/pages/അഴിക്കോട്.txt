=അഴിക്കോട്=

കണ്ണൂര്‍ നഗരത്തിനു സമീപമുള്ള ഒരു തീരദേശഗ്രാമം. 16.041 ച.കി.മീ. വിസ്തീര്‍ണവും: 45,951 (2001); ജനങ്ങളുമുള്ള അഴിക്കോട് പഞ്ചായത്തിന്റെ വ. വളപട്ടണം പുഴയും പ. അറബിക്കടലും തെ.കി. ചിറയ്ക്കല്‍ പഞ്ചായത്തുമാണ് അതിര്‍ത്തികള്‍ നിര്‍ണയിക്കുന്നത്.

ജനസംഖ്യയില്‍ ഭൂരിഭാഗവും നെയ്ത്തുകാരും കൃഷിക്കാരുമാണ്. വലുതും ചെറുതുമായ നിരവധി നെയ്ത്തുശാലകളുണ്ട്. ഇവിടെ ഉത്പാദിപ്പിക്കപ്പെടുന്ന തുണിത്തരങ്ങള്‍ക്കു വിദേശവിപണികളില്‍പ്പോലും പ്രചാരമുണ്ട്. പഞ്ചായത്തിന്റെ വ.ഭാഗത്തുള്ള വളപട്ടണം പുഴയില്‍ തടിക്കച്ചവടത്തിനു പ്രാധാന്യമുണ്ട്. 

അറബിക്കടലും വളപട്ടണംപുഴയും ചേരുന്ന സംഗമസ്ഥാനത്ത് അഴിയുള്ളതിനാലാകണം 'അഴിക്കോട്' എന്ന പേരു സിദ്ധിച്ചത്. അഴിമുഖം സ്ഥിതിചെയ്ത പ്രദേശം (അഴീക്കല്‍) തുറമുഖത്തിന് അനുയോജ്യമാണ്. അഴീക്കല്‍ എന്ന സ്ഥലം മത്സ്യബന്ധനകേന്ദ്രവും കച്ചവടകേന്ദ്രവുമാണ്. അരയന്മാര്‍ ഇടതിങ്ങി ത്താമസിക്കുന്ന മത്സ്യബന്ധനകേന്ദ്രമാണ് കടലോരങ്ങള്‍.

വിവിധ മതവിഭാഗങ്ങളുടെ ആരാധനാകേന്ദ്രങ്ങള്‍, ഇവിടെ പലതുണ്ട്. ഇവയില്‍ അക്ളിയത്ത് അമ്പലവും (ശിവക്ഷേത്രം) പുതിയകാവ് അമ്പലവും (ഭഗവതിക്ഷേത്രം) പഴക്കമേറിയവയാണ്. അഴിക്കോട് വയലില്‍ സ്ഥിതിചെയ്യുന്ന 'വന്‍കുളം' പ്രസിദ്ധമാണ്. വിസ്താരമേറിയതും മനോഹരവുമായ ശിലകള്‍ ചെത്തിയെടുത്ത് നിര്‍മിക്കപ്പെട്ട പടവുകളോടുകൂടിയ ഈ കുളം പ്രാചീന ശില്പകലാവൈഭവത്തിനുദാഹരണമാണ്.