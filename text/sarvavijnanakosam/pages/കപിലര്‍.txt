
== കപിലര്‍ ==

സംഘകാല തമിഴ്‌ കവികളില്‍ പ്രമുഖന്‍. തൊല്‌കാപ്പിയരുടെ സമകാലികനായ ഇദ്ദേഹത്തിന്റെ കവിതകള്‍ സംഘകാവ്യസമാഹാരങ്ങളില്‍ ധാരാളമായി ഉദ്ധരിക്കപ്പെട്ടിട്ടുണ്ട്‌. പാരി എന്ന ചിറ്റരചനുമായുള്ള സുദൃഢ സുഹൃദ്‌ബന്ധം കപിലരുടെ പല കവിതകളിലും അനുസ്‌മരിച്ചിട്ടുണ്ട്‌. തമിഴകത്തെ "മൂവേന്തന്മാരെ' പരിഗണിക്കാതെ കപിലര്‍ പാരിയുടെ അപദാനങ്ങള്‍ വാഴ്‌ത്തുകയും ഉജ്ജ്വലമായ കവിതയിലൂടെ അദ്ദേഹത്തിന്റെ നാമം ശാശ്വതീകരിക്കയും ചെയ്‌തു. തന്റെ രക്ഷാധികാരി യുദ്ധത്തില്‍ വധിക്കപ്പെട്ടപ്പോള്‍ കപിലര്‍ പരേതന്റെ അനാഥരായ പെണ്‍മക്കളുടെ വിവാഹം ഉറപ്പിച്ചശേഷം തീയില്‍ ചാടി മരിച്ചു. സമകാലികര്‍ ഇദ്ദേഹത്തെപ്പറ്റി സ്‌നേഹാദരങ്ങളോടുകൂടിയാണ്‌ പരാമര്‍ശിച്ചിട്ടുള്ളത്‌. ഇദ്ദേഹത്തിന്റെ ഏറ്റവും പ്രൗഢമായ കൃതി ഒരു ആര്യരാജകുമാരനെ കാവ്യസങ്കേതങ്ങള്‍ പഠിപ്പിക്കാന്‍ വേണ്ടി എഴുതിയ കുറിഞ്ചിപ്പാട്ടാണ്‌. ഇദ്ദേഹത്തിന്റെ മറ്റ്‌ വിശിഷ്ടകലാസൃഷ്ടികള്‍ ലഘു കവിതകളായിട്ടാണ്‌ ലഭിച്ചിട്ടുള്ളത്‌. മറ്റ്‌ ഏതൊരു ഭാരതീയ കവിയെയും അതിശയിക്കാന്‍ പോരുന്ന തരത്തിലുള്ള പ്രതിരൂപകല്‌പനകള്‍ നിറഞ്ഞതാണ്‌ കപിലരുടെ കാവ്യഭാവന. ഈ കവിയുടെ പ്രതീകസങ്കല്‌പങ്ങളെല്ലാം ധീരവും യഥാതഥവും അകൃത്രിമവുമായിരുന്നു. ഇദ്ദേഹത്തിന്റെ പ്രമഗാനങ്ങളുടെ മഹിമാതിശയത്തിഌം മാധുര്യത്തിഌം ആസ്‌പദമായിട്ടുള്ളത്‌ ഈ ബിംബവിധാനമായിരുന്നു എന്നു പറയാം. വിശദാംശങ്ങളില്‍ ശ്രദ്ധയും നിരീക്ഷണപാടവവും ഉള്ള കറയറ്റ പ്രകൃത്യാരാധകനാണ്‌ ഇദ്ദേഹം. പുറംകവിതകളിലെ അഥവാ പ്രമേതരവിഷയങ്ങളായ കവിതകളിലെ ആത്മനിഷ്‌ഠ സ്വഭാവം, അകം കവിതകളില്‍ (പ്രമകവിതകളില്‍) ആകണ്‌ഠം മുഴുകിയ ഒരു സംപൂജ്യവ്യക്തിത്വത്തെ ചിത്രീകരിക്കാന്‍ നമ്മെ സഹായിക്കുന്നു.

കപില നാമധാരികളായി മറ്റനേകം കവികള്‍ ഉണ്ടായിരുന്നു. പൂര്‍വസംഘകാല കവികളില്‍പ്പെട്ട തൊല്‌കാപ്പിയര്‍ അഥവാ വൃദ്ധകപിലര്‍ ഇക്കൂട്ടത്തില്‍ ശ്രദ്ധേയനാണ്‌ (നോ: തൊല്‌കാപ്പിയര്‍).

മൂന്നാമത്തെ കപിലര്‍ സംഘകാലാനന്തരം ജീവിച്ച കവിയാണ്‌. സംഘം ക്ലാസ്സിക്കുകളില്‍ ഉള്‍പ്പെടുത്തിയിട്ടുള്ള കുറിഞ്ചിക്കളി (നോ: കലിത്തൊകൈ)യുടെ കര്‍ത്താവാണ്‌ ഇദ്ദേഹം. പതിനെണ്‍കീഴ്‌കണക്ക്‌ എന്ന സമാഹാരത്തില്‍പ്പെട്ട ഇന്നനാര്‍ പത്ത്‌ എന്ന പ്രബോധനാത്മകമായ കൃതിയുടെ രചയിതാവിനെപ്പറ്റി ഭിന്നാഭിപ്രായം നിലവിലിരിക്കുന്നു എങ്കിലും ഇത്‌ എഴുതിയത്‌ കപിലര്‍ തന്നെയാകാനാണ്‌ സാധ്യത എന്നു ഗവേഷകന്മാര്‍ കരുതുന്നു.

നാലാമത്തെ കപിലര്‍ മതപരമായ വിഷയങ്ങളെ ആസ്‌പദമാക്കി കവിതകളെഴുതിയിരുന്നയാളാണ്‌. ആ കവിതകള്‍ പതിനൊന്നാം ശൈവതിരുമുറൈയില്‍ അഥവാ പ്രമാണഗ്രന്ഥത്തില്‍ ചേര്‍ത്തിട്ടുണ്ട്‌. തന്നിമിത്തം കപിലനായനാര്‍ എന്ന പേര്‍ അദ്ദേഹത്തിനു സിദ്ധിച്ചു.

അതിപ്രസിദ്ധമായ കപിലര്‍ അകവത്‌ എന്ന ഗ്രന്ഥത്തിന്റെ പ്രണേതാവാണ്‌ അഞ്ചാമത്തെ കപിലര്‍. ഈ ഗ്രന്ഥത്തില്‍ മനുഷ്യജീവിതത്തിന്റെ  അസ്ഥിരത കണക്കിലെടുത്ത്‌ ജാതികൃതമായ അസമത്വങ്ങള്‍ക്കെതിരായ തീവ്രവികാരങ്ങള്‍ ലളിതവും ശക്തവുമായ ഭാഷയില്‍ ആവിഷ്‌കരിച്ചിരിക്കുന്നു. 

(പ്രാഫ. ജെ. യേശുദാസന്‍)