= അനോഫെലിസ്  =

Anopheles


ഡിപ്റ്റിറ (Diptera) ഗോത്രത്തില്‍ ക്യൂലിസിഡേ കുടുംബത്തില്‍പ്പെടുന്ന കൊതുകുകളുടെ ഒരു ജീനസ്. മുപ്പതിലേറെ സ്പീഷീസ് ഈ ജീനസിലുണ്ട്. അനോഫെലിസ് കൊതുകുകളുടെ ചിറകില്‍ കറുത്ത പൊട്ടുകള്‍ കാണാം. ഇവ വിശ്രമിക്കുന്ന സമയത്ത് തല താഴ്ത്തി ശരീരം തറനിരപ്പിനു ലംബമായാണ് വയ്ക്കുക. ഇതിനാല്‍ തറനിരപ്പിനു സമാന്തരമായി ശരീരം കാണുന്ന ക്യൂലക്സുകളില്‍നിന്നും അനോഫെലിസുകളെ നിഷ്പ്രയാസം തിരിച്ചറിയാന്‍ സാധിക്കും. മലമ്പനിക്കു കാരണമായ പ്ളാസ്മോഡിയത്തിന്റെ ജീവിതചക്രം പരിപൂര്‍ണമാകാന്‍ ഈ കൊതുകും മനുഷ്യനും കൂടിയേതീരൂ. സര്‍ റൊണാള്‍ഡ് റോസാണ് ആദ്യമായി മലമ്പനി പരത്തുന്നതില്‍ കൊതുകിനുള്ള പങ്ക് കണ്ടുപിടിച്ചത്. പ്ളാസ്മോഡിയത്തിന്റെ വിവിധ ജീവിതദശകളില്‍ ആദ്യപകുതി അനോഫെലിസിന്റെ ശരീരത്തില്‍ കഴിയുന്നു.


<gallery Caption="അനോഫെലിസിന്‍റ ജീവിതദശകള്‍
1.മുട്ട 2.ലാര്‍വ 3.പ്യൂപ്പ 4.കൊതുക് ">
Image:p.no.519(1.1).jpg
Image:p.no.519(1.2).jpg
Image:p.no.519(2).jpg
Image:p.no.519(3).jpg
</gallery>

അനോഫെലിസിന്റെ എല്ലാ സ്പീഷീസിന്റെയും മുട്ടകള്‍ ഒറ്റയൊറ്റയായാണ് കാണപ്പെടുക. ദീര്‍ഘവൃത്താകൃതിയിലുള്ള ഓരോ മുട്ടയുടെയും ഇരുവശങ്ങളിലായി കാറ്റുനിറച്ച ഒരു പ്രത്യേകഭാഗം കാണാം. ഇതുള്ളതിനാല്‍ മുട്ടകള്‍ വെള്ളത്തില്‍ പൊങ്ങിക്കിടക്കും. മുട്ട വിരിഞ്ഞുണ്ടാകുന്ന ലാര്‍വകളെ 'കൂത്താടികള്‍' എന്നു വിളിക്കുന്നു. വലിയ തലയും ഖണ്ഡങ്ങള്‍ ചേര്‍ന്നുണ്ടായ ശരീരവുമുള്ള കൂത്താടി തികച്ചും ഒരു ജലജീവിയാണെങ്കിലും അന്തരീക്ഷ വായുവാണ് ശ്വസിക്കുന്നത്. സൈഫണ്‍ എന്നറിയപ്പെടുന്ന ഒരു കുഴലിലൂടെ ഇത് വായു വലിച്ചെടുക്കുന്നു. അനോഫെലിസില്‍ ഈ സൈഫണ്‍ വളരെ ചെറുതായിരിക്കും. ജലനിരപ്പിനു തൊട്ടു താഴെ സമാന്തരമായാണ് കൂത്താടി കാണപ്പെടുക. ലാര്‍വ വളര്‍ന്ന് ഉറയുരിക്കലിനുശേഷം പ്യൂപ്പ (Pupa) ആയിത്തീരുന്നു. മറ്റെല്ലാ ഷഡ്പദങ്ങളുടെയും പ്യൂപ്പയില്‍നിന്നും വ്യത്യസ്തമാണ് കൊതുകിന്റേത്. ആഹാരം കഴിക്കുക എന്നതൊഴിച്ച് ലാര്‍വ ചെയ്യുന്ന എല്ലാ പ്രവൃത്തികളും കൊതുകിന്റെ പ്യൂപ്പയും ചെയ്യും. ഉരോഭാഗത്തുള്ള രണ്ടു ട്യൂബുകളാണ് ഇവയുടെ ശ്വസനാവയവങ്ങള്‍. രണ്ടോ മൂന്നോ ആഴ്ചകള്‍ക്കുള്ളില്‍ ഇത് പൂര്‍ണ വളര്‍ച്ചയെത്തിയ കൊതുകായി മാറും.
[[Category:ജന്തുശാസ്ത്രം]]