=തമസ്സ്=

1. ഗുണത്രയത്തിലൊന്ന്. സത്ത്വം, രജസ്സ്, തമസ്സ് എന്നിവയാണ് ഗുണത്രയം. ഗുരുത, അജ്ഞത, ഭ്രമം, തൃഷ്ണ, കോപം, അലസത മുതലായവയ്ക്കു കാരണമാകുന്ന ഗുണമാണ് തമസ്സ്.

'അല്പചേതസ്സാമവന്നുംതമസ്സൊ-

ക്കെയകന്നു പോയ്.'

എന്ന് മഹാഭാരതത്തില്‍ പരാമര്‍ശിച്ചിരിക്കുന്ന തമസ്സിന് അജ്ഞത എന്നാണ് അര്‍ഥം.
 
'വിശ്വസ്ഥിതിപ്രളയസൃഷ്ടിക്കു സത്വ-
 
രജസ്തമോഭേദ! ഹരിനാരായണായനമഃ'

എന്ന് ഹരിനാമകീര്‍ത്തനത്തിലും
 
'പോകാസക്തി തമഃ പ്രകാശ
 
ശബളശ്രീയൊത്തമദ്ധ്യോര്‍വിയില്‍.'

എന്ന് കുമാരനാശാന്‍ പ്രരോദനത്തിലും തമസ്സിന്റെ പ്രഭാവം പ്രകടമാക്കിയിട്ടുണ്ട്.
 
തമസ്സിന് അന്ധകാരം എന്ന അര്‍ഥവും കാണുന്നു:
 
'മുങ്ങിപ്പൊങ്ങും തമസ്സിന്‍കടലിലൊരു
 
കുടംപോലെ ഭൂചക്രവാളംമുങ്ങിപ്പോയ്.'

എന്ന് ഒരു വിലാപത്തില്‍ വി.സി.ബാലകൃഷ്ണപ്പണിക്കര്‍ വര്‍ണിച്ചിരിക്കുന്നത് ഈ അര്‍ഥത്തിലാണ്. തമസ്സ്, മോഹം, മഹാമോഹം, താമിസ്രം, അന്ധതാമിസ്രം എന്നിങ്ങനെ അവിദ്യയ്ക്ക് 5 പ്രഭേദങ്ങള്‍ കല്പിച്ചിട്ടുള്ളതായി പദ്മപുരാണത്തില്‍ പറഞ്ഞിരിക്കുന്നു. അവിദ്യ, മായ, പാപം, ദുഃഖം, ചേറ്, നരകം, മരണം, ശ്രവസ്സിന്റെ പുത്രന്മാരിലൊരാള്‍, ദക്ഷപുത്രന്‍, പൃഥുശ്രവസ്സിന്റെ പുത്രന്‍, രാഹു എന്നീ അര്‍ഥങ്ങളും തമസ്സ് എന്ന പദത്തിനുള്ളതായി നിഘണ്ടുകാരന്മാര്‍ വ്യക്തമാക്കിയിട്ടുണ്ട്.

2. ഹിന്ദി നോവല്‍. ഭീഷ്മസാഹ്നി(1915-2003)യാണ് രചയിതാവ്. 

സ്വാതന്ത്യസമര സേനാനിയും നോവലിസ്റ്റും ചെറുകഥാകൃത്തും നാടകകൃത്തുമായിരുന്ന ഭീഷ്മസാഹ്നി തമസ്സിന്റെ രചനയോടെയാണ് പ്രശസ്തനായത്. അതിനു മുമ്പ് യശ്പാലിന്റെ ഝൂഠാ സച് പോലെ അപൂര്‍വം കൃതികള്‍ മാത്രമേ ഹിന്ദിയില്‍ ഇന്ത്യാ-പാകിസ്ഥാന്‍ വിഭജനത്തെ ഇതിവൃത്തമാക്കി രചിക്കപ്പെട്ടിരുന്നുള്ളൂ. ഇന്ത്യയിലെ വര്‍ഗീയ ലഹളകളുടെ ഭീതിദമായ ചിത്രം കാഴ്ചവയ്ക്കുന്ന തമസ്സ് 1975-ല്‍ പ്രസിദ്ധീകൃതമായി. ഇന്ത്യാ-പാകിസ്ഥാന്‍ വിഭജനത്തോടെ ഹിന്ദു-മുസ്ളിം വര്‍ഗീയത അഴിഞ്ഞാടിയതിന്റെ ചിത്രമാണ് ഈ നോവലിലുള്ളത്. ഇന്ത്യയുടെ നിലനില്പിനുതന്നെ ഭീഷണിയായ ഒരു വലിയ പ്രശ്നത്തിന്റെ സര്‍ഗാവിഷ്കരണമാണ് തമസ്സ്. വര്‍ഗീയ ലഹളകളില്‍ അകപ്പെട്ടവരുടെ അനുഭവങ്ങള്‍ കഥാരൂപത്തില്‍ ഈ കൃതിയില്‍ നിബന്ധിച്ചിരിക്കുന്നു. രണ്ടുഭാഗങ്ങളില്‍ 21 അധ്യായങ്ങളിലായി ഇതിവൃത്തം വിഭജിച്ചിട്ടുണ്ട്. കലാപത്തിന്റെ ആരംഭം മുതല്‍ സമാധാനയാത്ര വരെയാണ് പ്രതിപാദ്യം. ദില്ലി മുതല്‍ പഞ്ചാബിലെ ഒരു മുസ്ളിം ഭൂരിപക്ഷജില്ലയില്‍ വരെ വര്‍ഗീയത എങ്ങനെ ആഴത്തില്‍ വേരോടിയിരിക്കുന്നു എന്ന നടുക്കുന്ന യാഥാര്‍ഥ്യമാണ് ഈ നോവലിലൂടെ അവതീര്‍ണമാകുന്നത്.

നത്ഥൂ  ഒരു ചെരുപ്പുകുത്തിയാണ്. ചത്ത മൃഗങ്ങളുടെ തോല്‍ ഉരിച്ചെടുത്ത് പാകപ്പെടുത്തുകയാണ് അയാളുടെ ജോലി. ഒരു ദിവസം രാത്രി ഒരു ഇടുങ്ങിയ തെരുവില്‍ വച്ച് അയാള്‍ ഒരു പന്നിയെക്കൊന്നു. മുറാദ് അലിയാണ് പന്നിയെ കൊല്ലാന്‍ അയാളോട് ആവശ്യപ്പെട്ടത്. അതിന്റെ വരുംവരായ്കകളൊന്നും നത്ഥൂ ഓര്‍ത്തില്ല. കടുത്ത ലീഗ് വാദിയായ മുറാദ് അലി പന്നിയെക്കൊന്നതിന് അയാള്‍ക്ക് അഞ്ചുരൂപ കൂലിയും കൊടുത്തു. പള്ളിമുറ്റത്ത് ആരോ പന്നിയെക്കൊന്നിട്ടിരിക്കുന്ന വിവരമറിഞ്ഞ ഇസ്ളാംമതക്കാര്‍ ഉടന്‍ ഇളകിവശായി. നഗരത്തിലെമ്പാടും സംഘര്‍ഷം പടര്‍ന്നുപിടിച്ചു.  ഉടനേ, പന്നിക്കു ബദലായി ഒരു പശുവും കൊല്ലപ്പെട്ടു. തുടര്‍ന്ന് എങ്ങും കൊല്ലും കൊലയും കൊള്ളിവയ്പും വ്യാപൃതമായി. കിംവദന്തികള്‍ക്കും പഞ്ഞമുണ്ടായില്ല. 24 മണിക്കൂറിനുള്ളില്‍ത്തന്നെ അനേകം കടകളും മനുഷ്യരും ചാമ്പലായി. നഗരത്തില്‍ ഹിന്ദു-മുസ്ളിം സംഘര്‍ഷമായി അത് പരിണമിച്ചു. നിരപരാധികളായ അനേകര്‍ ഭയത്തോടെ കഴിഞ്ഞുകൂടി. മനുഷ്യനിലെ മൃഗം ഉണര്‍ന്നു പ്രവര്‍ത്തിക്കുന്നത് എല്ലായിടത്തും ദൃശ്യമായി.    

[[Image:Bheesma.jpg|250x200px|thumb|left]]

നോവലിന്റെ രണ്ടാം ഭാഗത്ത് ലഹള അടുത്ത ഗ്രാമങ്ങളിലേക്കും പടരുന്നതിന്റെ ആഖ്യാനമാണുള്ളത്. വിഭജനത്തിനുമുമ്പു തന്നെ ഇന്ത്യയില്‍ വിഭജനാനുകൂലികള്‍ ശക്തിയാര്‍ജിച്ചിരുന്നു. ഇടക്കാല സര്‍ക്കാരില്‍ മുസ്ളിംലീഗും ചേര്‍ന്നെങ്കിലും അവര്‍ വിഭജനമെന്ന ആവശ്യത്തില്‍ ഉറച്ചു നില്ക്കുകയാണുണ്ടായത്. ഈ ചരിത്രഭൂമികയിലാണ് ഭീഷ്മസാഹ്നി തന്റെ നോവല്‍ശില്പം വാര്‍ത്തിരിക്കുന്നത്. ഡെപ്യൂട്ടി പൊലീസ് കമ്മീഷണര്‍, ഭാര്യ ലീജാ, കോണ്‍ഗ്രസ് സ്വയംസേവകന്‍ ജര്‍നൈല്‍, മുസ്ളിം യുവാവ് ശാഹന്‍ വാജ് തുടങ്ങിയവരാണ് നോവലിലെ മുഖ്യ കഥാപാത്രങ്ങള്‍. ഇതിലെ പല കഥാപാത്രങ്ങളും മതത്തിനും ജാതിക്കും രാഷ്ട്രീയത്തിനും ഉപരി മനുഷ്യത്വത്തിന് വലിയ വില കല്പിക്കുന്നവരാണ്. മാനവികത ഉയര്‍ത്തി പിടിക്കുന്നവര്‍ എല്ലാമതങ്ങളിലും രാഷ്ട്രീയപാര്‍ട്ടികളിലുമുണ്ട്. എന്നിട്ടും ഇന്ത്യയുടെ മണ്ണില്‍ സംഘര്‍ഷം തുടര്‍ന്നും സംഭവിച്ചുകൊണ്ടിരിക്കുന്നു. തീവ്രവാദികള്‍ എണ്ണത്തില്‍ കുറവെങ്കിലും രാഷ്ട്രശരീരത്തെ കാര്‍ന്നുതിന്നുന്ന കാന്‍സറായി വളരാന്‍ അവര്‍ക്ക് കഴിയുന്നു. നോവലിസ്റ്റ് തമസ്സില്‍ ഒരിടത്ത് എഴുതുന്നു: 'ഓരോ കലാപവും രാഷ്ട്രശരീരത്തില്‍ ആഴത്തിലുള്ള ഓരോ മുറിവാണ് ഉണ്ടാക്കുന്നത്. അവയുടെ പാട് ഒരിക്കലും മായില്ല.' കലാപങ്ങളില്‍ മരിക്കുന്നവര്‍ ഏറെയും നിരപരാധികളാണ്. കലാപം കൊണ്ട് ഒരു പ്രശ്നത്തിനും പരിഹാരം ആവുകയുമില്ല. ചുരുക്കത്തില്‍ കാലഘട്ടത്തിന്റെ ആവശ്യകതയായ മതനിരപേക്ഷതയുടെ കൊടി ഉയര്‍ത്തിപ്പിടിക്കുന്നു  എന്നതാണ്  തമസ്സിന്റെ  എക്കാലത്തേയും പ്രസക്തി.        

ഇന്ത്യയുടെ സാംസ്കാരിക-സാഹിത്യരംഗത്ത് ഒരു വലിയ ശബ്ദമായിരുന്ന ഭീഷ്മസാഹ്നി യഥാര്‍ഥ ദേശീയവാദിയായിരുന്നു. ഇപ്റ്റ, സഹമത് തുടങ്ങിയ പ്രസ്ഥാനങ്ങളിലൂടെ സാമൂഹിക മാറ്റത്തിനു ശ്രമിച്ച അദ്ദേഹത്തിന്റെ ഈ നോവല്‍ ഗോവിന്ദ് നിഹ്ലാനി ദൂരദര്‍ശന്‍  സീരിയല്‍ ആക്കിയതോടെ കൂടുതല്‍ ശ്രദ്ധിക്കപ്പെട്ടു. 1980-കളുടെ അവസാനം തമസ്സിന്റെ ചലച്ചിത്രാവിഷ്കാരവും ഉണ്ടായി. ഇത് പ്രശ്നത്തിന്റെ ഗൗരവത്തിലേക്കും ഹിന്ദു-മുസ്ളിം ഐക്യത്തിന്റെ അടിയന്തര ആവശ്യകതയിലേക്കും ശ്രദ്ധതിരിക്കാന്‍ പര്യാപ്തമായി. തമസ്സിന്  1975-ലെ സാഹിത്യ അക്കാദമി അവാര്‍ഡ് ലഭിച്ചിരുന്നു. സോവിയറ്റ്ലാന്‍ഡ് നെഹ്റു അവാര്‍ഡ്, ലോട്ടസ് അവാര്‍ഡ് തുടങ്ങിയവ സാഹ്നിക്ക് ലഭിക്കുന്നതിനും  തമസ്സ്  സഹായകമായി.  അപര്‍ണാസെന്നിന്റെ മിസ്റ്റര്‍ ആന്‍ഡ് മിസ്സിസ്സ് അയ്യര്‍ എന്ന സിനിമയില്‍ ദേശസ്നേഹിയായ ഒരു മുസ്ളിമിന്റെ റോളില്‍ ഭീഷ്മസാഹ്നി അഭിനയിക്കുകയും ചെയ്തിട്ടുണ്ട്.