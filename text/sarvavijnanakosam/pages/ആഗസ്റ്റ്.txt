=ആഗസ്റ്റ്=
August

ആധുനിക കലണ്ടര്‍ അനുസരിച്ചുള്ള പന്ത്രണ്ട് ഇംഗ്ളീഷ് മാസങ്ങളില്‍ എട്ടാമേത്തത്. കര്‍ക്കടകം-ചിങ്ങം കാലമാണ് ആഗസ്റ്റ് (ആഗസ്ത്). മാര്‍ച്ച് മുതല്‍ കണക്കാക്കുന്ന പ്രാചീന റോമന്‍ കലണ്ടറില്‍ ആറാമത്തെ മാസമാണിത്. ബി.സി. 8 വരെ സെക്സ്റ്റൈലിസ് (Sextills) എന്നായിരുന്നു ഇതിനു പേര്‍. റോമന്‍ ചക്രവര്‍ത്തി സെക്സ്റ്റൈലിസ് സ്വന്തം ജീവിതത്തിലെ അവിസ്മരണീയ സംഭവങ്ങള്‍ നടന്ന മാസമെന്ന നിലയില്‍ ഇതിന് തന്റെ പേര്‍ നിര്‍ദേശിച്ചു. അഗസ്തസ് സ്വന്തം പേരിന്റെ സ്മരണയ്ക്കായി ഇതിന് ആഗസ്ത് എന്ന് പേര്‍ മാറ്റിയിട്ടു. ജൂലിയസ് സീസറെ ബഹുമാനിച്ചുകൊണ്ടാണ് ജൂലായ് എന്ന പേരുണ്ടായത്. ജൂലായ്ക്ക് 31 ദിവസമാണ്. അതേ ബഹുമതി അഗസ്തസിനും നല്കിക്കൊണ്ട് അന്നത്തെ റോമന്‍ ഭരണസമിതി (Senate) ആഗസ്തിനും 31 ദിവസം തന്നെ നിശ്ചയിച്ചു. ഫെബ്രുവരിയില്‍നിന്ന് 1 ദിവസം എടുത്തിട്ടാണ് ഇതിന് 31 ആക്കിയത്. സീസര്‍ തന്നെ മാസങ്ങളുടെ ദൈര്‍ഘ്യം നിശ്ചയിച്ചു എന്നുംഅഭിപ്രായമുണ്ട്.


ഉത്തരാര്‍ധഗോളത്തില്‍ ചൂടും ആര്‍ദ്രതയും അനുഭവപ്പെടുന്ന കാലമാണ് ആഗസ്റ്റ്. ഇംഗ്ളണ്ടിലും സ്കോട്ട്ലന്‍ഡിലും 'ഹാര്‍വസ്ററ് ഹോം' എന്ന പേരില്‍ ആഗ. 1 ആഘോഷിക്കപ്പെടുന്നു. ദക്ഷിണാര്‍ധഗോളത്തിലാകട്ടെ, ശൈത്യകാലത്തിന്റെ അവസാനമാണ് ആഗസ്റ്റ്; വസന്തത്തിന്റെ ആരംഭവും.

1947 ആഗ. 15-ന് ആണ് ബ്രിട്ടീഷ് ആധിപത്യത്തില്‍നിന്നും ഇന്ത്യ രാഷ്ട്രീയസ്വാതന്ത്ര്യം നേടിയത്. ആഗസ്റ്റില്‍ ഏറെ രാഷ്ട്രങ്ങള്‍ സ്വതന്ത്രമായി. ഉദാ. ബൊളീവിയ (1825), ഉറുഗ്വേ (1825), തെക്കന്‍ കൊറിയ (1948). ഭരണഘടനയുടെ 19-ാം ഭേദഗതി (ആഗ. 26) അനുസരിച്ച് യു.എസ്സില്‍ സ്ത്രീകള്‍ക്ക് വോട്ടവകാശം ലഭിച്ചത് (1920) ആഗസ്റ്റിലാണ്. ഇതേ മാസത്തിലാണ് അമേരിക്ക കണ്ടെത്താന്‍ കൊളംബസ് യാത്ര (1492) ആരംഭിച്ചതും.