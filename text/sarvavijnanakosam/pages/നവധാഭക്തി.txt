=നവധാഭക്തി=

ഒന്‍പത് വിധത്തിലുള്ള ഭക്തി. 'നവം' എന്നത് ഒന്‍പതിനെ സൂചിപ്പിക്കുന്നു. ഹൈന്ദവവിശ്വാസമനുസരിച്ച് ഭക്തി ഒന്‍പത് തരത്തിലുണ്ട്-ശ്രവണം, കീര്‍ത്തനം, സ്മരണം, പാദസേവനം, അര്‍ച്ചനം, വന്ദനം, ദാസ്യം, സഖ്യം, ആത്മനിവേദനം എന്നിവയാണവ.

ഈശ്വരനാമങ്ങള്‍, സ്തോത്രങ്ങള്‍, ചരിത്രങ്ങള്‍ എന്നിവ കേള്‍ക്കുന്നതാണ് ശ്രവണം. കേട്ടറിഞ്ഞ കാര്യങ്ങള്‍ പ്രകീര്‍ത്തിക്കുന്നതാണ് കീര്‍ത്തനം. സങ്കീര്‍ത്തനം, ഇതിഹാസപുരാണപാരായണം, ഭജനാര്‍ച്ചനകള്‍ എന്നിവ ഇതില്‍ ഉള്‍പ്പെടുന്നു. സദാ ഈശ്വരനെ ഓര്‍ത്തുകൊണ്ടിരിക്കുക എന്നതാണ് സ്മരണം. ആദ്യത്തെ രണ്ടെണ്ണം ഭക്തിയിലേക്കെത്താനുള്ള പ്രാഥമിക മാര്‍ഗങ്ങളാണെങ്കില്‍, ഭക്തി ഉണ്ടായിക്കഴിഞ്ഞ് ആദ്യമുണ്ടാകുന്നതാണ് സ്മരണം. ഈശ്വരപാദപൂജയാണ് പാദസേവനം കൊണ്ടുദ്ദേശിക്കുന്നത്. അത് കേവലമായ വിഗ്രഹപൂജയല്ല; ഈശ്വരസൃഷ്ടിയായ പ്രപഞ്ചത്തെ സേവിക്കല്‍ കൂടിയാണ്.

ഈശ്വരാര്‍ച്ചന തന്നെയാണ് അര്‍ച്ചന. ഭഗവാനെയും ഭഗവാന്റെ സൃഷ്ടികളായ എല്ലാറ്റിനെയും ഭക്തിപൂര്‍വം വണങ്ങുന്നതാണ് വന്ദനം. ഈശ്വരനെ സ്വാമി അഥവാ യജമാനനായി കണ്ട് ദാസനെപ്പോലെ ജീവിക്കലാണ് ദാസ്യം. ഈശ്വരനെ തന്റെ കൂട്ടുകാരനെപ്പോലെ കണ്ട് പെരുമാറാന്‍ കഴിയുന്ന ഭക്തിയുടെ ഒരു സമുന്നതതലമാണ് സഖ്യം. ഈശ്വരനില്‍ തന്നെത്തന്നെ സമര്‍പ്പിക്കലാണ് ആത്മനിവേദനം. അനുക്രമം ഉയരുന്ന ഭക്തിയുടെ വിവിധ തലങ്ങളാണ് നവധാഭക്തി എന്ന സങ്കല്പത്തിലൂടെ അനാവൃതമാകുന്നത്. ഇതില്‍നിന്ന് വ്യത്യസ്തമായ മറ്റൊരു ഭക്തിസങ്കല്പമാണ് ഏകാദശഭക്തി.

നവധാഭക്തിയുടെ ഓരോരോ ഭാവങ്ങളുടെ വിശദീകരണങ്ങളായ സ്വാതിതിരുനാള്‍ കൃതികളാണ് നവരത്നമാലികാ കീര്‍ത്തനങ്ങള്‍.

(എം. സുരേഷ്)