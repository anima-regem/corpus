=അര്‍ഥാന്തരന്യാസം =
ഒരു അര്‍ഥാലങ്കാരം. പ്രസ്തുതമായ 'സാമാന്യ'ത്തെ അപ്രസ്തുതമായ 'വിശേഷം'കൊണ്ടോ, പ്രസ്തുതമായ 'വിശേഷ'ത്തെ അപ്രസ്തുതമായ 'സാമാന്യം' കൊണ്ടോ സമര്‍ഥിക്കുന്നതാണ് ഇതിന്റെ സ്വഭാവം. 

'സാമാന്യം താന്‍ വിശേഷം താ-

നിവയില്‍ പ്രസ്തുതത്തിന് 

അര്‍ഥാന്തരന്യാസമാകു-

മന്യംകൊണ്ടു സമര്‍ഥനം' (ഭാഷാഭൂഷണം) 

പ്രസ്തുത സമര്‍ഥനത്തിനായി അര്‍ഥാന്തരത്തെ-അപ്രസ്തുതത്തെ-ന്യസിക്കുന്നത് (വയ്ക്കുന്നത്) എന്ന നിലയില്‍ പേരിന് അര്‍ഥയോജന. 

ഉദാ. 	1.'ചെറുപ്പകാലങ്ങളിലുള്ള ശീലം 

മറക്കുമോ മാനുഷനുള്ള കാലം;

കാരസ്കരത്തിന്‍ കുരു പാലിലിട്ടാല്‍

കാലാന്തരേ കയ്പു ശമിപ്പതുണ്ടോ.'

ആദ്യവാക്യം സാമാന്യമായ പ്രസ്തുതം; അടുത്ത വാക്യം വിശേഷമായ അപ്രസ്തുതം. 

2.'മല്ലാക്ഷീമണിയാള്‍ക്കു വല്ക്കലമിതും 

ഭൂയിഷ്ഠശോഭാവഹം;

നല്ലാകാരമതിന്നലങ്കരണമാ-

മെല്ലാപദാര്‍ഥങ്ങളും.'

ആദ്യവാക്യം വിശേഷപ്രസ്തുതവും അനന്തരവാക്യം സാമാന്യമായ അപ്രസ്തുതവും. 

സാമാന്യകഥനങ്ങളുടെ ഹേതുവാണ് വിശേഷകഥനവാക്യാര്‍ഥം. അതുകൊണ്ട് അര്‍ഥാന്തരന്യാസം ഒരു തരത്തില്‍ കാവ്യലിംഗം തന്നെയാണ് എന്ന് ഒരു പക്ഷമുണ്ട് (കാവ്യലിംഗം അലങ്കാരമേ അല്ല എന്നു ചില ആലങ്കാരികന്മാര്‍ കരുതുന്നു). 

ശ്ലേഷസാമ്യങ്ങളാകുന്ന അലങ്കാരബീജങ്ങളില്‍ ഒന്ന് ഉണ്ടെങ്കിലേ അര്‍ഥാന്തരന്യാസത്തിന് അലങ്കാരത്വമുള്ളു എന്നും 

കേവല വസ്തുസ്ഥിതികഥനരൂപമായ അര്‍ഥാന്തരന്യാസത്തിന് അലങ്കാരത്വം പറഞ്ഞുകൂടെന്നും ആണ് ചില ആലങ്കാരികന്മാരുടെ അഭിപ്രായം. 

'നീരാഹരിപ്പാനിഹ തേക്കുകാള 

പാരാതെ പിന്നോട്ടു നടന്നിടുന്നു;

ആരാകിലും ജീവനലാഭയത്നം 

നേരായ മുന്നോട്ടു ഗതിക്കു വിഘ്നം'

ഇവിടെ ശ്ലേഷമാണ് ബീജം; 'ചെറുപ്പകാലങ്ങളില്‍' ഇത്യാദി പദ്യത്തില്‍ സാമ്യച്ഛായയും. 

(കെ.കെ. വാധ്യാര്‍)