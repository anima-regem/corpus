
== കന്ദപദ്യം ==

തെലുഗുഭാഷയിലെ കന്ദവൃത്തത്തില്‍ രചിച്ച പദ്യം. തെലുഗിലെ പ്രാചീന കാവ്യങ്ങളില്‍ മിക്കവയും സംസ്‌കൃതവൃത്തങ്ങളുടെ "ജാതി'കളിലും "ഉപജാതി'കളിലും  രചിക്കപ്പെട്ടവയാണ്‌. "ജാതി'യില്‍പ്പെടുന്ന ഒരു വൃത്ത വിശേഷമാണ്‌ കന്ദം. കന്ദവൃത്തത്തിന്‌ ഒന്നും മൂന്നും പാദങ്ങളില്‍ മൂന്നു ഗണങ്ങള്‍ വീതവും രണ്ടും നാലും 
പാദങ്ങളില്‍ അഞ്ചുഗണങ്ങള്‍ വീതവും ഉണ്ടായിരിക്കണം. ഇതില്‍ തെലുഗുഗണകല്‌പനയനുസരിച്ചുള്ള നല ( ്‌ ്‌ ്‌ ് = നാലു ലഘു), നഗ ( ് ്- ് = മൂന്നു ലഘുവും ഒടുവില്‍ ഗുരുവും), 
ഭ (് ്- = ആദ്യം ഒരു ഗുരു പിന്നെ രണ്ടു ലഘു), ജ ( ് ് = ആദ്യം ലഘു, പിന്നെ ഗുരു, ഒടുവില്‍ ലഘു), സ ( ് ്- = ആദ്യം രണ്ടു ലഘു, ഒടുവില്‍ ഗുരു) എന്നീ ഗണങ്ങള്‍ മാത്രമേ ഉപയോഗിക്കാവൂ. രണ്ടും നാലും പാദങ്ങളില്‍ മൂന്നാമത്തെ ഗണം "നല' ഗണമോ "ജ' ഗണമോ ആയിരിക്കണം. പ്രസ്‌തുത പാദങ്ങളില്‍ നാലാമത്തെ ഗണത്തിന്റെ ആരംഭം യതി സ്ഥാനമായിരിക്കും. പാദാന്ത്യം "സ'ഗണമോ രണ്ടു ഗുരുക്കളോ (  = "ഗഗ'ഗണം) ആയിരിക്കണം. പ്രാസവും നിര്‍ബന്ധമാണ്‌. ഉദാ.
 <nowiki>
ശ്രീരാമുനി ദയചേതനു
ആരുഡിക സകല ജനുലു നൗരായനഗാ
ധാരാളമൈന നീതുലു
നോരൂരഗചവുലുപുട്ട നുഡിവേദ സുമതീ
 </nowiki>
(സുമതിശതകം  ബദ്‌ദെന)

കന്ദത്തെ പ്രാകൃതത്തിലുള്ള ഗാഥാച്ഛന്ദസ്സിനു സമമായി പല പണ്ഡിതന്മാരും ഗണിച്ചുപോരുന്നു. ഗാഥയില്‍ നാലു പാദങ്ങളിലും കൂടി സാധാരണയായി 54 മാത്രകളാണുള്ളത്‌; ഏതുതരമായാലും മൊത്തം 64 മാത്രകളില്‍ കൂടുതല്‍ ഉണ്ടാവുകയുമില്ല. കന്ദത്തിലും 64 മാത്രകള്‍ ഉള്ള പദ്യങ്ങള്‍ ഉണ്ട്‌. നന്നയ്യാ(തെലുഗുവിലെ ആദികവി 11-ാം ശ.)യുടെ പദ്യങ്ങളില്‍ പകുതിയും (1,219)  കന്ദപദ്യങ്ങളാണ്‌. അതുപോലെ നന്നിചോട(12-ാം ശ.)ന്‍െറ കവിതകളില്‍ 
534ഉം തിക്കന(13-ാം ശ.)യുടെ രചനയില്‍ 5,313ഉം കന്ദപദ്യങ്ങള്‍ തന്നെ. ഇതില്‍ നിന്നും പ്രാചീന കാലത്ത്‌ കന്ദപദ്യത്തിനു പ്രചുരപ്രചാരം സിദ്ധിച്ചിരുന്നു എന്ന്‌ വ്യക്തമാവുന്നു. "കന്ദപദ്യമെഴുതാനാവുന്ന കവിയാണ്‌ യഥാര്‍ഥകവി' എന്ന ചൊല്ലുതന്നെ തെലുഗുവിലുണ്ട്‌. ഇക്കാലത്തും പല യാഥാസ്ഥിതിക കവികളും കന്ദപദ്യത്തില്‍ത്തന്നെയാണ്‌ സാഹിത്യസപര്യ ചെയ്യുന്നത്‌. 

(നാ. ഭക്തവത്സല റെഡ്ഡി)