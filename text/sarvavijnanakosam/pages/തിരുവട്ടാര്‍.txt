=തിരുവട്ടാര്‍= 

[[Image:Thiruvattar(864).jpg|thumb|right|തിരുവട്ടാര്‍ ആദികേശവ ക്ഷേത്രം]]
തമിഴ്നാട്ടില്‍ കന്യാകുമാരി ജില്ലയില്‍ കല്‍ക്കുളം താലൂക്കില്‍ സ്ഥിതിചെയ്യുന്ന സ്ഥലം. തിരുവട്ടാര്‍ പണ്ട് തിരുവിതാംകൂര്‍ സംസ്ഥാനത്തിന്റെ ഭാഗമായിരുന്നു. 1860-ല്‍ ഉമയമ്മ റാണിയുടെ ഭരണകാലത്ത് മുഗള്‍ സര്‍ദാര്‍ ദക്ഷിണ വേണാട് ആക്രമിച്ചു. കോട്ടയം കേരളവര്‍മയുടെ സഹായത്തോടുകൂടി റാണി മുകിലപ്പടയെ പരാജയപ്പെടുത്തിയത് തിരുവട്ടാറില്‍ വച്ചാണ്. തിരുവട്ടാറില്‍ അതിപുരാതനമായ ഒരു വിഷ്ണുക്ഷേത്രമുണ്ട്. ഇവിടത്തെ പ്രതിഷ്ഠ ആദികേശവപ്പെരുമാള്‍ ആണ്.

നാഗരി, തമിഴ്, വട്ടെഴുത്ത് എന്നിങ്ങനെ പല ലിപികളിലായി കൊത്തിയിട്ടുള്ള ഇരുപതോളം ശിലാരേഖകള്‍ തിരുവട്ടാര്‍ ക്ഷേത്രത്തിലുണ്ട്. 1173-ല്‍ രചിക്കപ്പെട്ട 'തിരുവട്ടാറ്റുപള്ളി കൊണ്ടറുളുകിന്റ പെരുമാള്‍' എന്നു തുടങ്ങുന്നതാണ് ഇവിടത്തെ ഏറ്റവും പഴക്കമുള്ള രേഖ. ഏതാണ്ട് എല്ലാ ശാസനങ്ങളുടേയും വിഷയം ക്ഷേത്രകാര്യങ്ങളാണ്.