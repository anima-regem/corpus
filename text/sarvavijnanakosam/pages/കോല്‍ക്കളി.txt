
== കോല്‍ക്കളി ==
[[ചിത്രം:Koladi-3.png‎|200px|right|thumb|കോല്‍ക്കളി]]
കേരളത്തില്‍ പ്രചാരത്തിലുള്ള ഒരു ഗ്രാമീണകല. വിഭിന്ന ജാതിമതസ്ഥരുടെ ഇടയിലുള്ള കോല്‍ക്കളികള്‍ക്കു തമ്മില്‍ സാരമായ വ്യത്യാസമുണ്ട്‌. ഈ കളിയുടെ രൂപഭാവങ്ങളിലും ശൈലിയിലും വൈവിധ്യം സൃഷ്‌ടിക്കുവാന്‍ ദേശഭേദം കൂടി കാരണമാണ്‌. കോലടിക്കളി, കമ്പടിക്കളി എന്നീ പേരുകളിലും ഇത്‌ അറിയപ്പെടുന്നു. മുസ്‌ലിങ്ങളുടെ പ്രധാന കായികവിനോദകലയാണിത്‌. ആയോധന പ്രധാനമായ ഇതിനു മെയ്‌വഴക്കവും, ചുവടു പരിശീലനവും, താളനിയന്ത്രണവും അവശ്യംവേണ്ട ഘടകങ്ങളാണ്‌. തലശ്ശേരി, കണ്ണൂര്‍ ജില്ലകളിലെ മാപ്പിളമാരുടെ കോല്‍ക്കളിയും പയ്യന്നൂര്‍ കോല്‍ക്കളിയും വളരെ പ്രസിദ്ധമാണ്‌. ചെറുമരും കോല്‍ക്കളി നടത്താറുണ്ട്‌. സംസ്ഥാന സ്‌കൂള്‍ കലോത്സവത്തിലെ ഒരു മത്സരയിനം കൂടിയാണിത്‌.

കളരിയഭ്യാസവുമായി ബന്ധമുള്ള ഇത്‌ പുരുഷന്മാരുടെ ഒരു കായികവിനോദമാണ്‌. കളരിയഭ്യാസത്തില്‍ ഏര്‍പ്പെടുന്നവര്‍ ആദ്യം കോല്‍ക്കളി അഭ്യസിക്കുന്നു. കളരിപ്പയറ്റു പഠിപ്പിക്കുന്ന ആശാനെയെന്നപോലെ, കോല്‍ക്കളി പഠിപ്പിക്കുന്ന ആശാനെയും കുരിക്കള്‍ (ഗുരുക്കള്‍) എന്നാണ്‌ വിളിക്കുന്നത്‌. കൈയും മെയ്യും കണ്ണും മനസ്സും ഒത്തൊരുമിച്ചു പിഴയ്‌ക്കാതെ പ്രവര്‍ത്തിച്ചാല്‍ മാത്രമേ കോല്‍ക്കളി വിജയിക്കൂ.

കോല്‍ക്കളിയുടെ ഉത്‌പത്തിയെക്കുറിച്ച്‌ ഉത്തരകേരളത്തില്‍ ചില ഐതിഹ്യങ്ങള്‍ നിലവിലുണ്ട്‌. ദ്രാണാചാര്യര്‍ സംവിധാനം ചെയ്‌തതാണു കോല്‍ക്കളി എന്നാണ്‌ വിശ്വാസം.

യാദവരുടെ ഒരു വിനോദ നര്‍ത്തനമായിരുന്നു കോലടി എന്നു വിശ്വസിക്കുന്നവരുമുണ്ട്‌.

മുണ്ടും ബനിയനുമാണ്‌ കോല്‍ക്കളി വേഷം. ചിലര്‍ കൈലിയും തലക്കെട്ടും ധരിക്കാറുണ്ട്‌. മതപരമായ ഒരനുഷ്‌ഠാനകലയല്ലെങ്കിലും പ്രാര്‍ഥനയോടുകൂടിയാണ്‌ കോല്‍ക്കളി സാധാരണയായി ആരംഭിക്കുന്നത്‌. അരങ്ങില്‍ ഒരു പീഠവും നിലവിളക്കും കാണും. ഇതിന്‌ ചുരുങ്ങിയത്‌ 16 കളിക്കാരെങ്കിലും വേണം. "വന്ദനക്കളി'യാണ്‌ ആദ്യത്തേത്‌. വട്ടക്കോല്‍, ചുറ്റിക്കോല്‍, തെറ്റിക്കോല്‍, ചിന്ത്‌, ചാഞ്ഞുകളി, ഇരുന്നുകളി, തടുത്തുകളി, താളക്കളി, ചുറഞ്ഞുചുറ്റല്‍, ചവിട്ടിച്ചുറ്റല്‍, കൊടുത്തോപോന്ന കളി, തടുത്തുതെറ്റിക്കോല്‍, ഒരുമണിമുത്ത്‌, ഒളവും പുറവും, ഒറ്റ കൊട്ടിക്കളി, രണ്ടു കൊട്ടിക്കളി, പിണച്ചു കൊട്ടിക്കളി, പുതിയ തെറ്റിക്കോല്‌, എണീറ്റുകളി, ചുറഞ്ഞു ചിന്ത്‌ എന്നിങ്ങനെ കോല്‍ക്കളിയെ പല ഇനമായി തിരിക്കാം. ഓരോ ഇനത്തിലും അഞ്ചോ അതില്‍ക്കൂടുതലോ ഉപകളികളുമുണ്ട്‌.

കളിക്കാരെ അകം, പുറം എന്നിങ്ങനെ രണ്ടായി തിരിക്കാം. ഓരോ കളിയുടെയും തുടക്കത്തില്‍ അകക്കളിക്കാര്‍ അകത്തും പുറക്കളിക്കാര്‍ പുറത്തുമായിരിക്കും. പിന്നീട്‌ ഓരോരുത്തരും സ്ഥാനം മാറിക്കളിക്കും. അതിനു കോര്‍ക്കല്‍ എന്നാണ്‌ പറയുന്നത്‌. കളിയുടെ അന്ത്യത്തില്‍ കളിക്കാര്‍ ആദ്യമുണ്ടായ സ്ഥലത്തുതന്നെ വന്നുചേരും.
ഓരോ കളിക്കും പ്രത്യേക താളക്രമമുണ്ട്‌. കുരിക്കള്‍ വായ്‌ത്താരിയായി താളങ്ങള്‍ പറയും. താളത്തിന്നനുഗുണമായ പാട്ടുകളാണു കളിക്കു പാടിവരുന്നത്‌. താളവും പാട്ടും മെയ്യഭ്യാസവും ഒത്തിരിക്കണം.

രാഗതാളസമന്വിതമായ ഗാനങ്ങളാണു കോലടിപ്പാട്ടുകള്‍. ഹിന്ദുക്കളുടെ കോലടിപ്പാട്ടുകളധികവും ഭക്തിഗീതങ്ങളും ക്ഷേത്രങ്ങളെ ചുറ്റിപ്പറ്റിയുള്ള കഥാഖ്യാനങ്ങളുമാണ്‌. ഇതിഹാസപുരാണ കഥാവലംബികളായ പാട്ടുകളും കുറവല്ല. ഉത്തര കേരളത്തില്‍ കോല്‍ക്കളിക്കു പാടാറുള്ള ഒരു ഗാനമാണു കലശപ്പാട്ട്‌. പയ്യന്നൂര്‍ പെരുമാള്‍ ക്ഷേത്രത്തിന്റെ നിര്‍മാണത്തെയും അലങ്കാരവിശേഷങ്ങളെയും വര്‍ണിക്കുന്ന പത്തോളം വൈവിധ്യമുള്ള ഗാനരീതികള്‍ കലശപ്പാട്ടില്‍ പ്രയുക്തമായിട്ടുണ്ട്‌. ആനിടില്‍ രാമനെഴുത്തച്ഛന്‍ എന്ന കവിയാണു കലശപ്പാട്ടിന്റെ കര്‍ത്താവെന്നു ചിലര്‍ വിശ്വസിക്കുന്നു. വേദാന്തവും ഭക്തിയും ശൃംഗാരവും ഫലിതവുമൊക്കെ കോല്‍ക്കളിപ്പാട്ടുകളില്‍ പ്രതിഫലിക്കുന്നുണ്ട്‌. സംസ്‌കൃതം, തമിഴ്‌, തുളു എന്നീ ഭാഷകളുടെ സ്വാധീനം ചില പാട്ടുകളില്‍ കാണാം. മുസ്‌ലിങ്ങളുടെ കോല്‍ക്കളിപ്പാട്ടുകളില്‍ അറബിപ്പദങ്ങളുടെ അതിപ്രസരം ദൃശ്യമാണ്‌.

കോല്‍ക്കളിക്കുവേണ്ടി മാത്രമായുള്ള പാട്ടുകളെക്കൂടാതെ ചില കഥകളിപ്പദങ്ങളും കിളിപ്പാട്ടുകളും വഞ്ചിപ്പാട്ടുകളും ഉപയോഗച്ചുവരുന്നുണ്ട്‌. മുസ്‌ലിങ്ങള്‍ ദപ്പുകളിക്കു പാടാറുള്ള പാട്ടുകളില്‍ ചിലവ കോല്‍ക്കളിക്കും പാടിവരുന്നു.

(ഡോ.എം.വി. വിഷ്‌ണു നമ്പൂതിരി)