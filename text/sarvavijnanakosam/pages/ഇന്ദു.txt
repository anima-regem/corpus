
== ഇന്ദു ==

ആയുര്‍വേദത്തിലെ ചില പ്രാമാണികഗ്രന്ഥങ്ങളുടെ വ്യാഖ്യാതാവ്‌. ചക്രപാണിദത്തന്‍, ഡല്‍ഹ(ല്ല)ണന്‍, ഹേമാദ്രി, ചന്ദ്രനന്ദനന്‍, അരുണദത്തന്‍ മുതലായവരുടെ പംക്തിയില്‍പ്പെടുന്ന ഇദ്ദേഹം വാഗ്‌ഭടന്റെ അഷ്‌ടാംഗസംഗ്രഹത്തിന്‌ ശശിലേഖ എന്നൊരു വ്യാഖ്യാനം എഴുതിയിട്ടുണ്ട്‌. ചരകം, സുശ്രുതം മുതലായ പ്രസിദ്ധ സംഹിതകളില്‍ രേഖപ്പെടുത്തിയിട്ടുള്ള ചികിത്സാ സിദ്ധാന്തങ്ങളെ സംഗ്രഹിച്ചും, പലേടത്തും കൂടുതല്‍ അര്‍ഥസ്‌ഫുടത വരുത്തിയും എഴുതിയ സുപ്രസിദ്ധഗ്രന്ഥമാണ്‌ അഷ്‌ടാംഗസംഗ്രഹം. ഈ ഒരു വ്യാഖ്യാനംകൊണ്ടുതന്നെ ഇദ്ദേഹം ലബ്‌ധപ്രതിഷ്‌ഠനായിത്തീര്‍ന്നു. ഇന്ദുവിനു മുമ്പും അഷ്‌ടാംഗസംഗ്രഹത്തിന്‌ പലരും വ്യാഖ്യാനങ്ങളെഴുതിയിട്ടുണ്ട്‌. അവയില്‍ ചിലത്‌ മൂലഗ്രന്ഥത്തെക്കാള്‍ ദുര്‍ഗ്രഹങ്ങളാണ്‌. ശശിലേഖയുടെ ആവിര്‍ഭാവത്തോടെ ആ വ്യാഖ്യാനങ്ങളെല്ലാം അസ്‌തപ്രഭങ്ങളായിത്തീര്‍ന്നു. ശശിലേഖാവ്യാഖ്യാനത്തിന്റെ തുടക്കത്തില്‍ 
കാണുന്ന പദ്യത്തില്‍ തന്നെ ഇതു സൂചിപ്പിച്ചിട്ടുണ്ട്‌. 
 <nowiki>
"ദുര്‍വ്യാഖ്യാ വിഷസുപ്‌തസ്യ
			വാഹടസ്യാസ്‌മദുക്തയ:
സന്തു സംവിത്തിദായിന്യഃ സദാഗമപരിഷ്‌കൃതാഃ'
				(അ. സം. സൂ. 1-1)
 </nowiki>
ഇന്ദുവിന്റെ ജീവിതകാലഘട്ടം ഏതെന്ന്‌ സൂക്ഷ്‌മമായി അറിവില്ല. ഇദ്ദേഹം വാഗ്‌ഭടന്റെ സമകാലീനനും അന്തേവാസിയും ശിഷ്യനുമായിരുന്നു. വാഗ്‌ഭടനെക്കുറിച്ചുള്ള ഒരു പ്രസിദ്ധ ധ്യാനശ്ലോകമുണ്ട്‌.
 <nowiki> 
"ലംബശ്‌മശ്രുകലാപമംബുജനിഭച്ഛായാദ്യുതിം വൈദ്യകാ-
നന്തേവാസിന ഇന്ദുജജ്ജടമുഖാനധ്യാപയന്തം സദാ
ആഗുല്‍ഫാമലകഞ്ചുകാഞ്ചിതദരാലക്ഷ്യോപവീതോജ്വലത്‌-
കണ്‌ഠസ്ഥാഗരുസാരമഞ്ചിതദൃശം ധ്യായേ ദൃഢം വാഗ്‌ഭടം'
 </nowiki>
ഈ പുരാതനപദ്യത്തിലെ "അന്തേവാസിനമിന്ദുജജ്ജടമുഖാനധ്യാപയന്തം സദാ' (അന്തേവാസികളായ ഇന്ദു, ജജ്‌ഭടന്‍ മുതലായവരെ എല്ലായ്‌പോഴും പഠിപ്പിച്ചുകൊണ്ടിരിക്കുന്ന) എന്ന്‌ വാഗ്‌ഭടനെ വിശേഷിപ്പിച്ചിരിക്കുന്നു. വാഗ്‌ഭടന്റെ കാലം ഏതെന്നും ഇനിയും സൂക്ഷ്‌മമായി നിര്‍ണയിക്കപ്പെട്ടിട്ടില്ല. പല തെളിവുകളെയും ആസ്‌പദമാക്കി എ.ഡി. നാലും എട്ടും നൂറ്റാണ്ടുകള്‍ക്കിടയ്‌ക്ക്‌ ആയിരിക്കാം എന്ന്‌ ചരിത്രകാരന്മാര്‍ക്കിടയില്‍ അഭിപ്രായമുണ്ട്‌. വാഗ്‌ഭടവിരചിതമായ അഷ്‌ടാംഗഹൃദയത്തിനും ശശിലേഖ എന്ന പേരില്‍ത്തന്നെ ഇദ്ദേഹം ഒരു വ്യാഖ്യാനം എഴുതിയിട്ടുണ്ട്‌. എന്നാല്‍ സൂത്രസ്ഥാനം, ശാരീരസ്ഥാനം, ചികിത്സാസ്ഥാനം എന്നിവയുടെ വ്യാഖ്യാനങ്ങള്‍ മാത്രമേ ഇപ്പോള്‍ പ്രചാരത്തിലുള്ളൂ. 

(ഡോ. പി.ആര്‍. വാര്യര്‍)