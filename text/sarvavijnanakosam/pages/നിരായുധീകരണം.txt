=നിരായുധീകരണം=

മാരാകായുധങ്ങളുടെ ഉന്മൂലനത്തെ ലക്ഷ്യമാക്കിയുള്ള സാര്‍വദേശീയ സമാധാനപ്രക്രിയ. ഒന്നും രണ്ടും ലോകയുദ്ധത്തിലുണ്ടായ മനുഷ്യക്കുരുതിയും നാശനഷ്ടങ്ങളുമാണ് നിരായുധീകരണം എന്ന പ്രക്രിയയെ രാഷ്ട്രീയപരമായി അനിവാര്യമാക്കിയത്.

നിരായുധീകരണമെന്നത് ഒരു പ്രക്രിയയും എത്തിച്ചേരേണ്ട ഒരു സ്ഥിതിവിശേഷവുമാണ്. ഒരു പ്രക്രിയയെന്ന നിലയില്‍ അതില്‍ ആയുധ വ്യൂഹങ്ങളില്‍ കുറവു വരുത്തല്‍, അവ ഇല്ലാതാക്കല്‍ എന്നിവയെല്ലാം ഉള്‍പ്പെടുന്നു. എത്തിച്ചേരേണ്ട സ്ഥിതിവിശേഷം ആയുധവിമുക്തമായ ഒരു ലോകമാണ്; പുനരായുധീകരണം സാധ്യമല്ലാത്ത ഒരു ലോകം.

നിരായുധീകരണം പ്രാദേശികമോ, സാര്‍വദേശീയമോ ആകാം. അത് പൂര്‍ണമോ, ഭാഗികമോ ആകാം. ചിലതരം ആയുധങ്ങളെപ്പറ്റി മാത്രമോ, എല്ലാത്തരങ്ങളെയും പൊതുവായി ബാധിക്കുന്നതോ ആകാം. നിരായുധീകരണ പ്രക്രിയയിലെ പദ്ധതികളില്‍ ഏറ്റവും സമഗ്രമായുള്ളത് "പൊതുവും പൂര്‍ണവുമായ നിരായുധീകരണം'' (General Disarmament) ആണ്. 1959-ല്‍ ഐക്യരാഷ്ട്രസംഘടന പ്രഖ്യാപിച്ചത് ഈ ലക്ഷ്യമാണ്.

രാഷ്ട്രകേന്ദ്രീകൃതമായ ആധുനിക വ്യവസ്ഥിതിയില്‍ നിരായുധീകരണം സാധാരണ നടപ്പാക്കുന്നത് ഒരു രാഷ്ട്രം യുദ്ധത്തില്‍ പരാജയപ്പെടുമ്പോഴാണ്. പരാജിതരാഷ്ട്രത്തെ വിജയിയായ രാഷ്ട്രം നിരായുധമാക്കും. യുദ്ധത്തെത്തുടര്‍ന്ന് ഉണ്ടാകുന്ന സമാധാന ഉടമ്പടി ഈ സ്ഥിതി തുടരുകയോ, പുനരായുധീകരണത്തിന് കര്‍ശന വ്യവസ്ഥകള്‍ ഏര്‍പ്പെടുത്തുകയോ ചെയ്യും.

ഇതില്‍ നിന്ന് തുലോം വിഭിന്നമാണ് പരമാധികാരതുല്യതയുള്ള രാഷ്ട്രങ്ങള്‍ തമ്മില്‍, യുദ്ധപശ്ചാത്തലത്തിലല്ലാതെ, കൂടിയാലോചനകളിലൂടെ, ഉണ്ടാകുന്ന നിരായുധീകരണ ഉടമ്പടികള്‍. സുരക്ഷയ്ക്കാവശ്യമായുള്ളത് ആയുധപ്പന്തയമല്ല, പരസ്പര സഹകരണമാണ് എന്ന ബോധ്യം ഇത്തരം ഉടമ്പടികള്‍ക്ക് ആധാരമാകുന്നു. വിലപേശലിനുള്ള കൂടിയാലോചനകളിലൂടെ, മെല്ലെപ്പോക്കിന്റെ തന്ത്രം ഉപയോഗിച്ച് ഉണ്ടാക്കുന്ന ഉടമ്പടികള്‍ ഭാഗികമോ, പരിമിതമോ ആയിരിക്കും. ഈ സമീപനം മിക്കപ്പോഴും എത്തിച്ചേരുന്നത് ആയുധനിയന്ത്രണത്തിലാണ്; നിരായുധീകരണത്തിലല്ല. ഇവ രണ്ടും - നിരായുധീകരണവും ആണവനിയന്ത്രണവും-ഒന്നാണെന്ന മട്ടിലാണ് പ്രബലരാഷ്ട്രങ്ങള്‍ സാധാരണ പെരുമാറുന്നതും, പ്രവര്‍ത്തിക്കുന്നതും.

[[Image:nira 2010 1.png]]

1932-34 കാലയളവില്‍ ലീഗ് ഒഫ് നേഷന്‍സിലെ അംഗരാജ്യങ്ങള്‍ അമേരിക്കയും റഷ്യയുമായി ജനീവയില്‍ വച്ചു നടത്തിയ ലോക നിരായുധീകരണ സമ്മേളനം (World Disarmament Conference), കിഴക്ക്-പടിഞ്ഞാറന്‍ രാഷ്ട്രങ്ങള്‍ക്ക് തുല്യപ്രാതിനിധ്യം നല്കിക്കൊണ്ട് 1960-ല്‍ ജനീവയില്‍ അരങ്ങേറിയ ദശരാഷ്ട്ര നിരായുധീകരണ കമ്മിറ്റി (Ten National Disamament Committe), ശീതകാലത്ത് അമേരിക്കയും സോവിയറ്റ് റഷ്യയുമായി ചര്‍ച്ചകള്‍ക്ക് സാധ്യതയൊരുക്കുവാന്‍ ഐക്യരാഷ്ട്രസഭയുടെ കാര്‍മികത്വത്തില്‍ 1962-68 കാലയളവില്‍ ജനീവയില്‍ വച്ചുനടന്ന പതിനെട്ട് രാഷ്ട്ര നിരായുധീകരണകമ്മിറ്റി, ഐക്യരാഷ്ട്രസഭയുടെ തന്നെ പ്രത്യേക താത്പര്യപ്രകാരം 1978-ല്‍ രൂപംകൊണ്ട കോണ്‍ഫറന്‍സ് ഓണ്‍ ഡിസ്ആമമന്റി(CD)ന്റെ ഭാഗമായുള്ള സമ്മേളനങ്ങള്‍ തുടങ്ങിയവ നിരായുധീകരണ പരമ്പരയിലെ ശ്രദ്ധേയമായ ശ്രമങ്ങളാണ്. ഇത്തരത്തിലുള്ള ഏതാനും അന്താരാഷ്ട്ര കൂടിയാലോചനകളുടെ ഭാഗമായി ആയുധനിയന്ത്രണ ഉടമ്പടികളും സാധ്യമായിട്ടുണ്ട്. നോ:ആയുധനിയന്ത്രണം

തന്ത്രപ്രധാനമായ ആയുധങ്ങളുടെ നിയന്ത്രണം ലക്ഷ്യമാക്കി അമേരിക്കയും സോവിയറ്റ് റഷ്യയും തമ്മില്‍ 1991 ജൂല. 31-ന് ഒപ്പുവച്ച സ്റ്റാര്‍ട്ട് (START-Strategic Arms Reduction Treaty) ഉടമ്പടി 1994 ഡി. 5-ന് നിലവില്‍ വന്നു. ബാലിസ്റ്റിക് മിസൈലുകള്‍, ദീര്‍ഘദൂര മിസൈലുകള്‍, ബോംബുകള്‍ എന്നിവയുടെ എണ്ണം 10,000-ത്തില്‍ നിന്നും 6,000-മായി കുറയ്ക്കുവാനും 2001-ഓടെ 80 ശ.മാ. ആണവായുധങ്ങള്‍ ഉന്മൂലനം ചെയ്യുവാനും പരസ്പര ധാരണയായി. പില്ക്കാലത്ത് സ്റ്റാര്‍ട്ട് I എന്നു പുനര്‍നാമകരണം ചെയ്ത പ്രസ്തുത ഉടമ്പടി 2009 ഡി. 5-ന് അവസാനിച്ചു. സ്റ്റാര്‍ട്ട് ക-ന്റെ പുന്തുടര്‍ച്ചയായി 1993 ജനു. 3-ന് റഷ്യയും അമേരിക്കയും തമ്മില്‍ ധാരണയിലെത്തിയ സ്റ്റാര്‍ട്ട് II, ദീര്‍ഘദൂര മിസൈലുകളുടെയും ആണവായുധങ്ങളുടെയും ശേഖരം 6,000-ത്തില്‍ നിന്നും 3500-3000 ആക്കി നിയന്ത്രിക്കുവാന്‍ വ്യവസ്ഥ ചെയ്യുന്നതായിരുന്നു. എന്നാല്‍ ആന്റി ബാലസ്റ്റിക് മിസൈല്‍ ഉടമ്പടിയില്‍ നിന്നും 2002 ജൂണ്‍ 14-ന് അമേരിക്ക പിന്‍വാങ്ങിയതോടെ സ്റ്റാര്‍ട്ട് II ലക്ഷ്യം കാണാതായി. ബാരക് ഒബാമ അമേരിക്കന്‍ പ്രസിഡന്റായതിനെത്തുടര്‍ന്ന് 2010 ഏപ്രിലില്‍ സ്റ്റാര്‍ട്ട് III അഥവാ പുതിയ സ്റ്റാര്‍ട്ട് ഉടമ്പടിയില്‍ അമേരിക്കയും റഷ്യയും ഒപ്പുവച്ചു. പത്തു വര്‍ഷത്തിനിടയില്‍ മൂന്നു ഘട്ടങ്ങളിലായി 60 ശ.മാ. ആണവായുധങ്ങളുടെ ഉന്മൂലനമാണ് പുതിയ സ്റ്റാര്‍ട്ടിലൂടെ വ്യവസ്ഥ ചെയ്തിട്ടുള്ളത്. നിരായുധീകരണത്തിനുള്ള ഇത്തരം ശ്രമങ്ങള്‍ നടന്നുവരുമ്പോള്‍ത്തന്നെയാണ് ഇറാഖ്, കൊസോവ, അഫ്ഗാന്‍, ലബനോന്‍ തുടങ്ങിയ രാഷ്ട്രങ്ങള്‍ക്കുമേല്‍ ഭീകരവാദത്തിന്റെ മറവില്‍ അമേരിക്കയുടെ നേതൃത്വത്തില്‍ സൈനിക നടപടികളും ആയുധപ്രയോഗങ്ങളും തുടര്‍ന്നുവരുന്നത് എന്നത് വൈരുധ്യമാണ്.

നിരായുധീകരണവുമായി ബന്ധപ്പെട്ട മൂന്നു കാര്യങ്ങള്‍ ചൂണ്ടിക്കാണിക്കാം. അവയൊന്നും "പൊതുവും പൂര്‍ണവുമായ നിരായുധീകരണ''ത്തിന്റെ ദിശയിലല്ല.

'''ഒന്ന്, ആണവ നിയന്ത്രണം.''' നിരായുധീകരണമെന്നതിന് പകരം ഈ പദം ഉപയോഗിച്ച് അവ രണ്ടും ഒന്നാണെന്ന പ്രതീതി വളര്‍ത്തുന്ന സമീപനമാണ് പൊതുവേ ശക്തമായ രാഷ്ട്രങ്ങളുടേത്. ഇത് ആശയക്കുഴപ്പമുണ്ടാക്കുന്നുവെന്ന് മാത്രമല്ല, നിരായുധീകരണത്തിന് തടസ്സമാകുകയും ചെയ്യുന്നു. ഈ നിലപാടിനെ പല പ്രമുഖ ചിന്തകരും വിമര്‍ശിച്ചിട്ടുണ്ട്.

[[Image:nira 2010.png]]

'''രണ്ട്, ആണവനിരായുധീകരണം.''' ഇത് മാത്രമാണ് നിരായുധീകരണമെന്നും, ഇതു മാത്രമേ ആവശ്യമുള്ളുവെന്നുമുള്ള ധാരണ പ്രബലവും, വ്യാപകവുമാണ്. പരമ്പരാഗതമെന്ന് വിശേഷിപ്പിക്കപ്പെടുന്ന ആയുധങ്ങളുടെ വര്‍ധമാനമായ ശക്തിയും, അവ ഉയര്‍ത്തുന്ന ഭീഷണിയും ഇവിടെ അവഗണിക്കുന്നു. ലക്ഷോപലക്ഷം ജനങ്ങളുടെ ജീവന്‍ കുരുതികൊടുത്തതും, പല രാജ്യങ്ങളെയും നശിപ്പിച്ചതും, ഇത്തരം ആയുധങ്ങള്‍ ഉപയോഗിച്ചു നടത്തിയ യുദ്ധങ്ങളാണ്. ആധുനിക സാങ്കേതികവിദ്യയിലൂടെ കൂടുതല്‍ വിനാശകരമായിക്കൊണ്ടിരിക്കുന്ന ഈ ആയുധങ്ങളെ നിരായുധീകരണത്തില്‍ നിന്ന് ഒഴിവാക്കുവാന്‍ പാടില്ല.

'''മൂന്ന്, ഏകപക്ഷീയ നിരായുധീകരണം.''' മറ്റു രാജ്യങ്ങളുമായുള്ള കൂടിയാലോചനകളില്ലാതെ ഏകപക്ഷീയമായി നിരായുധീകരണ നടപടികള്‍ രാഷ്ട്രങ്ങള്‍ സ്വീകരിക്കുന്നതാണിത്. ഒരു രാജ്യത്തിന് പ്രചരണപരമായ നേട്ടങ്ങള്‍ ഉണ്ടാക്കാനും, മറ്റൊരു രാജ്യത്തിന്റെ നിലപാട് നിരായുധീകരണത്തിന് സഹായകമല്ലെന്ന് വരുത്തിത്തീര്‍ക്കാനും ഇത്തരം നടപടികള്‍ ചിലപ്പോള്‍ ഉപയോഗിക്കാറുണ്ട്.

ആയുധപ്പന്തയവും, അതിനോടുള്ള സമീപനവുമാണ് പൊതുവും പൂര്‍ണവുമായ നിരായുധീകരണത്തിന് വേണ്ടി വാദിക്കുന്നവരും ആയുധനിയന്ത്രണത്തിനായി വാദിക്കുന്നവരും തമ്മില്‍ വ്യത്യാസമുണ്ടാക്കുന്നത്. ആയുധവ്യൂഹങ്ങള്‍ക്ക് സാര്‍വദേശീയ കാര്യങ്ങളില്‍ ഒരു കര്‍ത്തവ്യവും പങ്കും ഉണ്ടെന്നാണ് ആയുധനിയന്ത്രണവാദികളുടെ നിലപാട്. അതേസമയം, ആയുധപ്പന്തയം സംഘര്‍ഷമുണ്ടാക്കുകയും യുദ്ധത്തിലേക്കു നയിക്കുകയും ചെയ്യുന്നുവെന്ന് നിരായുധീകരണം ആവശ്യപ്പെടുന്നവര്‍ ചൂണ്ടിക്കാണിക്കുന്നു.

നിരായുധീകരണത്തിന്റെ ഒരു ലക്ഷ്യം രാഷ്ട്രങ്ങളുടെ വര്‍ധമാനമായ സൈനികവത്കരണത്തെ തടയുകയെന്നതായിരിക്കണം. ആയുധവ്യൂഹങ്ങള്‍ കുറയ്ക്കുവാനും ഇല്ലാതാക്കാനുമുള്ള പ്രക്രിയ്ക്ക് രാഷ്ട്രീയവും നയതന്ത്രപരവും സാമ്പത്തികവും സാങ്കേതികവുമായ മാനങ്ങളുണ്ടായിരിക്കും. വിഭിന്ന തന്ത്രപര പരിഗണനകള്‍ ഈ പ്രക്രിയയില്‍ ആവശ്യമെന്ന് വ്യക്തം. രാഷ്ട്രീയ, സാമ്പത്തിക വ്യവസ്ഥിതികളാണ് ആയുധവത്കരണത്തെ സഹായിക്കുന്നതും നിലനിര്‍ത്തുന്നതും. നിരായുധീകരണം ആ വ്യവസ്ഥിതികളില്‍ മാറ്റമുണ്ടാക്കും. ഈ സാധ്യത തന്നെ നിരായുധീകരണത്തിന് പ്രതിബന്ധമാണ്.

മറ്റു വ്യവസായങ്ങളെ അപേക്ഷിച്ച് ആയുധവ്യവസായത്തിനു മുന്‍ഗണന നല്കുകയും ആയുധവ്യവസായവും വ്യാപാരവും സമ്പത്ക്രമത്തിന്റെ മുഖ്യഘടകമാക്കുകയും ചെയ്യുന്ന രാഷ്ട്രങ്ങളാണ് നിരായുധീകരണ ശ്രമങ്ങള്‍ക്കു തുരങ്കം വയ്ക്കുന്നത്. വിദേശ കാര്യങ്ങളില്‍ സൈനിക കാര്യങ്ങള്‍ക്ക് പ്രാധാന്യം നല്കുന്നവരില്‍ അധികാരം കേന്ദ്രീകരിക്കുന്നത് നിരായുധീകരണത്തിന് വിഘാതമാണ്. ആവശ്യമായ രാഷ്ട്രീയ ഇച്ഛാശക്തി പ്രതിഫലിപ്പിക്കുന്ന തന്ത്രങ്ങളാണ് നിരായുധീകരണത്തെ ഒരു കേവല സങ്കല്പമെന്ന നിലയില്‍ നിന്ന് സാധിതപ്രായമായ ഒരു ലക്ഷ്യത്തിലേക്കു എത്തിക്കുക.

നിരായുധീകരണത്തിലെത്തിയ ഒരു ലോകത്തിന് കേന്ദ്രീകൃതവും ശക്തവുമായ ഒരു അന്താരാഷ്ട്ര സംവിധാനം ആവശ്യമായിരിക്കും. രാഷ്ട്രങ്ങള്‍ മുന്‍കൈയെടുത്തുണ്ടാക്കുന്ന പൊതുവും പൂര്‍ണവുമായ നിരായുധീകരണത്തിന്റെ വ്യക്തമായ വിവക്ഷകളിലൊന്ന് ലോകത്തില്‍ സമാധാനവും സുരക്ഷയും ഉറപ്പുവരുത്താനുള്ള കഴിവും അധികാരവും ഈ അന്താരാഷ്ട്ര സംവിധാനത്തിനുണ്ടായിരിക്കണമെന്നുള്ളതാണ്.

ആണവ നിരായുധീകരണമെന്നത് അര്‍ഥമാക്കുന്നത് ആണവായുധങ്ങള്‍ ഇല്ലാതാക്കുന്നതാണ്. ഒരു ആണവായുധ വിമുക്തലോകമാണ് ലക്ഷ്യം. ഇവിടെയും ആയുധനിയന്ത്രണവാദികള്‍ നിരായുധീകരണത്തെ ആണവ നിയന്ത്രണമായി പരിമിതപ്പെടുത്തുന്നു. ലോകത്തെ മുഴുവന്‍ നാല്പതോ അന്‍പതോ അതിലധികമോ തവണ നശിപ്പിക്കാനുള്ള ശേഷി ഇന്നത്തെ ആണവായുധ ശേഖരത്തിനുണ്ടെന്നുള്ള വസ്തുത പരിഗണിക്കുമ്പോഴാണ് ആണവായുധങ്ങളുടെ എണ്ണം കുറയ്ക്കുക മാത്രം ചെയ്യുന്ന ആയുധ നിയന്ത്രണത്തിന്റെ അപര്യാപ്തത വ്യക്തമാകുക.

മനുഷ്യരാശിക്കും പ്രകൃതിക്കും ആണവായുധങ്ങള്‍ ഉയര്‍ത്തുന്ന ഭീഷണിയുടെയും വ്യാപ്തിയുടെയും സൂചന മാത്രമായിരുന്നു ഹിരോഷിമയും നാഗസാക്കിയും. അതുകൊണ്ടാണ് രണ്ടാം ലോകയുദ്ധത്തിന്റെ അവസാനത്തില്‍ത്തന്നെ, ആണവായുധങ്ങളുടെ ഉന്മൂലനം എന്ന ആവശ്യം ഉയര്‍ന്നത്. ശീതസമരത്തിലെ എല്ലാ കക്ഷികള്‍ക്കും ആണവായുധങ്ങള്‍ ഉയര്‍ത്തുന്ന ഭീഷണിയെപ്പറ്റി ബോധ്യമുണ്ടായിരുന്നു. അന്താരാഷ്ട്രതലത്തില്‍ ആണവനിരായുധീകരണത്തിനും നിര്‍വ്യാപനത്തിനുമുള്ള പല കരാറുകളുമുണ്ടായി.

പ്രധാനപ്പെട്ട ചില ആണവ ഉടമ്പടികള്‍ ഇവിടെ ചൂണ്ടിക്കാണിക്കുന്നു.

ഒന്ന്, ഭാഗിക പരീക്ഷണ നിരോധന ഉടമ്പടി (Partial Test Ban Treaty,P.T.B.T. പി.റ്റി.ബി.റ്റി.). 1963-ല്‍ പ്രാബല്യത്തില്‍വന്ന ഈ ഉടമ്പടി അന്തരീക്ഷത്തിലും ബാഹ്യാകാശത്തിലും ജലത്തിനടിയിലുമുള്ള ആണവപരീക്ഷണങ്ങള്‍ നിരോധിച്ചു; ഭൂഗര്‍ഭപരീക്ഷണങ്ങള്‍ അനുവദിച്ചുകൊണ്ട്. 

രണ്ട്, ആണവനിര്‍വ്യാപന ഉടമ്പടി (Non-Proliteration Treaty,N.P.T). 1968-ല്‍ ഉണ്ടാക്കി. 1970-ല്‍ പ്രാബല്യത്തില്‍ വന്നു. മൂന്ന് അടിസ്ഥാനപ്രമാണങ്ങളാണ് എന്‍.പി.റ്റി.ക്ക് ഉള്ളത്: ആണവ നിര്‍വ്യാപനം, നിരായുധീകരണം, ആണവസാങ്കേതികവിദ്യയുടെ സമാധാനപരമായ ഉപയോഗത്തിനുള്ള അവസരം.

ഏറ്റവും അധികം ചര്‍ച്ച ചെയ്യപ്പെടുന്നതും വിയോജിപ്പുകള്‍ നിലനില്ക്കുന്നതുമായ ഉടമ്പടി എന്‍.പി.റ്റിയാണ്. ആണവനിരായുധീകരണത്തെ സംബന്ധിച്ച 1995-ലെ ഉടമ്പടി സ്ഥിരമാക്കിയെങ്കിലും അഞ്ചു വര്‍ഷത്തിലൊരിക്കല്‍ ഇത് പുനരവലോകനം ചെയ്യപ്പെടുന്നു.

ആണവായുധ രാഷ്ട്രങ്ങളെ എന്‍.പി.റ്റി. നിര്‍വചിച്ചിട്ടുണ്ട്. ഒരു ആണവായുധമോ, മറ്റ് ആണവ സംവിധാനമോ, 1967 ജനു. 1-ന് മുമ്പ് സ്ഫോടനം നടത്തിയ രാഷ്ട്രങ്ങളാണ് അവ. അമേരിക്ക, സോവിയറ്റ് യൂണിയന്‍ (പിന്‍ഗാമി റഷ്യ), ബ്രിട്ടന്‍, ഫ്രാന്‍സ്, ചൈന എന്നീ രാഷ്ട്രങ്ങള്‍ക്കാണ് ഈ പദവി. പുതിയ രാഷ്ട്രങ്ങള്‍ ആണവായുധം ഉണ്ടാക്കുന്നത് തടയുകയെന്നതായിത്തീര്‍ന്നു ഉടമ്പടിയുടെ ഉദ്ദേശ്യം. ആണവരാഷ്ട്രങ്ങളുടെ ആണവപ്രവര്‍ത്തനങ്ങളില്‍-പരീക്ഷണം, നിര്‍മാണം, ശേഖരത്തിന്റെ വികാസം-ഒരു നിയന്ത്രണവും ഏര്‍പ്പെടുത്തിയില്ല. രാഷ്ട്രങ്ങളെ ഈ ഉടമ്പടി രണ്ടായി വിഭജിച്ചു: ആണവായുധരാഷ്ട്രങ്ങള്‍, ആണവായുധേതര രാഷ്ട്രങ്ങള്‍. പുതിയ അസമത്വത്തിന് അന്താരാഷ്ട്ര നിയമസാധുത നല്ക്പ്പെട്ടു. ഇതിനെ ഇന്ത്യ ശക്തിയായി എതിര്‍ത്തു. എന്‍.പി.റ്റി.യില്‍ ഇന്ത്യ ഒപ്പിട്ടിട്ടില്ല.

എന്‍.പി.റ്റിയുടെ ആറാം അനുഛേദം  ആണവായുധരാഷ്ട്രങ്ങള്‍ നിരായുധീകരണത്തിന് നടപടികള്‍ എടുക്കണമെന്ന് വ്യക്തമായി വ്യവസ്ഥ ചെയ്യുന്നു. ഇതിന് ആണവായുധ രാഷ്ട്രങ്ങള്‍ തയ്യാറാകാത്തത് എന്‍.പി.റ്റിയിലെ പ്രതിസന്ധിയുടെ പ്രധാന കാരണമാണ്.

മൂന്ന്, 1996-ല്‍ ഒപ്പുവച്ച സമഗ്ര പരീക്ഷണ നിരോധന ഉടമ്പടി (Comprehensive Test Ban Treaty,C.T.B.T. സി.റ്റി.ബി.റ്റി.), ഈ ഉടമ്പടി ഇതുവരെ പ്രാബല്യത്തില്‍ വന്നിട്ടില്ല. എല്ലാ പരിതഃസ്ഥിതികളിലുമുള്ള പരീക്ഷണങ്ങളെ പൂര്‍ണമായി നിരോധിക്കുന്നതാണ് ഈ ഉടമ്പടി. ഈ ഉടമ്പടിക്കു വേണ്ടി വളരെയേറെ പ്രയത്നിച്ച രണ്ടു രാഷ്ട്രങ്ങളാണ് അമേരിക്കയും ഇന്ത്യയും. അമേരിക്കന്‍ കോണ്‍ഗ്രസ്സും ഇതുവരെ ഉടമ്പടി അംഗീകരിച്ചിട്ടില്ല; ഇന്ത്യ ഒപ്പുവച്ചിട്ടുമില്ല.

പൂര്‍ണമായ ആണവ നിരായുധീകരണം നടത്തിയ ഒരു രാഷ്ട്രമുണ്ട്-ദക്ഷിണാഫ്രിക്ക. നെല്‍സണ്‍ മണ്ടേലയുടെ നേതൃത്വത്തില്‍ അധികാരത്തില്‍വന്ന പ്രഥമ ജനാധിപത്യ ഗവണ്‍മെന്റ്, ആദ്യം എടുത്ത തീരുമാനങ്ങളിലൊന്ന് അപ്പാര്‍ത്തീഡ് ഗവണ്‍മെന്റുണ്ടാക്കിയ ആണവായുധങ്ങള്‍ നശിപ്പിക്കുവാനുള്ളതായിരുന്നു.

സ്വതന്ത്രരാഷ്ട്രമായതുമുതല്‍ സാര്‍വത്രികവും വിവേചനരഹിതവും ഫലപ്രദവുമായ ആഗോളനിരായുധീകരണത്തിനുവേണ്ടിയാണ് ഇന്ത്യ നിലകൊള്ളുന്നത്. പൊതുവും പൂര്‍ണവുമായ നിരായുധീകരണത്തിലേക്കുള്ള ആദ്യപടിയെന്ന നിലയില്‍, മുന്‍ഗണന നല്കേണ്ടത് ആണവ നിരായുധീകരണത്തിനായിരിക്കണമെന്ന് ഇന്ത്യ വാദിക്കുന്നു. എല്ലാവിധ ആണവ പരീക്ഷണങ്ങളും പൂര്‍ണമായി നിരോധിക്കണമെന്ന് ആദ്യം ആവശ്യപ്പെട്ട രാജ്യം ഇന്ത്യയായിരുന്നു. 1954-ലാണ് ഇന്ത്യ ഈ ആവശ്യം ഐക്യരാഷ്ട്രസംഘടനയില്‍ ഉന്നയിച്ചത്. ഭാഗികപരീക്ഷണനിരോധന ഉടമ്പടി(പി.റ്റി.ബി.റ്റി.)ക്കായി പ്രവര്‍ത്തിച്ച രാജ്യങ്ങളില്‍ പ്രമുഖസ്ഥാനത്തായിരുന്നു ഇന്ത്യ.

1988-ല്‍ യു.എസ്സിന്റെ നിരായുധീകരണത്തിനുള്ള പ്രത്യേക സമ്മേളനത്തില്‍ ഇന്ത്യയുടെ പ്രധാനമന്ത്രി രാജീവ്ഗാന്ധി അവതരിപ്പിച്ച നിര്‍ദേശങ്ങള്‍-സമഗ്രപദ്ധതിയുള്‍പ്പെടെ ലോകശ്രദ്ധ ആകര്‍ഷിച്ചു.

ആണവായുധങ്ങളെ എന്നും എതിര്‍ത്തിരുന്ന ഇന്ത്യ 1998 മേയ് മാസത്തില്‍ ഏതാനും ആണവ സ്ഫോടനങ്ങള്‍ നടത്തി ആണവശക്തിയായി സ്വയം പ്രഖ്യാപിച്ചു. വിശദീകരണങ്ങളും നീതീകരണങ്ങളുമെന്തൊക്കെ ആയിരുന്നാലും, 1947 മുതല്‍ ഇന്ത്യ സ്വീകരിച്ചിരുന്ന നയത്തിനു വിരുദ്ധമായിരുന്നു അത്.

ആണവ നിരായുധീകരണം സംബന്ധിച്ച രണ്ടു പ്രധാന ഉടമ്പടികളില്‍-എന്‍.പി.റ്റി., പി.റ്റി.ബി.റ്റി.-നേരത്തെ സൂചിപ്പിച്ചതു പോലെ ഇന്ത്യ ഒപ്പുവച്ചിട്ടില്ല. ആണവായുധ ശക്തിയായശേഷം ഇവയില്‍ ഒപ്പുവയ്ക്കാന്‍ ഇന്ത്യയുടെ മേല്‍ സമ്മര്‍ദം വര്‍ധിച്ചിട്ടുണ്ട്.

നിരായുധീകരണ പ്രക്രിയയെ പ്രയാസപൂര്‍ണമാക്കുന്ന ഒരു വിരോധാഭാസം യു.എന്‍. ചാര്‍ട്ടറില്‍ത്തന്നെയുണ്ടെന്ന് ചൂണ്ടിക്കാണിക്കപ്പെടുന്നു. "വരുവാനിരിക്കുന്ന തലമുറകളെ യുദ്ധത്തിന്റെ കൊടും യാതനകളില്‍ നിന്ന് രക്ഷിക്കുവാന്‍, ലോകത്തിലെ മാനുഷികവും സാമ്പത്തികവുമായ വിഭവങ്ങളെ അതീവപരിമിതമായ തോതില്‍ മാത്രം ചെലവഴിച്ച്, സാര്‍വദേശീയ സമാധാനവും സുരക്ഷയും സ്ഥാപിക്കുക''യെന്നതാണ് യു.എന്‍. രക്ഷാസമിതിയുടെ കര്‍ത്തവ്യം. അതേസമയം തന്നെ രാഷ്ട്രങ്ങള്‍ക്കു സ്വയം പ്രതിരോധത്തിനുള്ള അവകാശം ചാര്‍ട്ടര്‍ അനുവദിക്കുന്നു. അങ്ങനെ ഏതു തോതിലുള്ള സൈനിക ചെലവിനെയും, എത്ര വലിയ ആയുധവത്കരണത്തെയും പ്രതിരോധത്തിന്റെ പേരില്‍ രാഷ്ട്രങ്ങള്‍ക്കു നീതീകരിക്കാം.

ഇതോടനുബന്ധിച്ച് പറയേണ്ട ഒരു കാര്യമുണ്ട്-ചാര്‍ട്ടര്‍ നടപ്പാക്കാന്‍ ബാധ്യതയുള്ള രക്ഷാസമിതിയിലെ സ്ഥിരാംഗങ്ങളെല്ലാം ആണവശക്തികളാണ്; ലോകത്തിലെ ഏറ്റവും വലിയ ആയുധ വ്യവസായികളും, വ്യാപാരികളും അവര്‍തന്നെ.

നിരായുധീകരണം എന്ന പദം ഇപ്പോള്‍ രാഷ്ട്രീയ നിഘണ്ടുവില്‍ നിന്ന് ഏതാണ്ട് അപ്രത്യക്ഷമായതായി തോന്നുന്നു. നിരായുധീകരണമെന്ന് പറയുമ്പോള്‍ മിക്കപ്പോഴും ആണവനിരായുധീകരണം മാത്രമാണ് അര്‍ഥമാക്കുക.

(നൈനാന്‍ കോശി)