=ടാങ്കര്‍=
Tanker

ദ്രാവക രൂപത്തിലുള്ള ചരക്കുകള്‍ കയറ്റിക്കൊണ്ടു പോകാനുപയോഗിക്കുന്ന കപ്പല്‍. മിക്കപ്പോഴും കപ്പലിന്റെ നീളത്തോളം വരുന്ന എണ്ണ ടാങ്കുകള്‍ ഇവയില്‍ കാണും. 

പൊതുവേ നാലു വിഭാഗം ടാങ്കറുകളുണ്ട്; എണ്ണ ടാങ്കറുകള്‍, രാസ പദാര്‍ഥങ്ങള്‍ കൊണ്ടു പോകാനുള്ള രാസവാഹക ടാങ്കറുകള്‍, ശീതീകരിച്ചു ദ്രാവക രൂപത്തിലുള്ള വാതകം കൊണ്ടു പോകുന്ന ടാങ്കറുകള്‍, വെള്ളം/അയിര്/ബള്‍ക്ക്/എണ്ണ/ധാന്യം എന്നിവ കൊണ്ടുപോകുന്ന 'OBO' (ore/bulk/oil) ടാങ്കറുകള്‍. 
[[Image:Tanker.png|200px|left|thumb|ടാങ്കര്]]
1886-ല്‍ ജര്‍മനിയിലാണ് ആദ്യ ടാങ്കര്‍ നിര്‍മിച്ചത്. ഇതിന് 300 മെട്രിക് ടണ്‍ എണ്ണ വഹിക്കാനുള്ള ശേഷി ഉണ്ടായിരുന്നു. രണ്ടാം ലോകയുദ്ധം പെട്രോളിയത്തിനുള്ള ആവശ്യം വ്യാപകമാക്കിയത് ടാങ്കര്‍ നിര്‍മാണത്തിനു പ്രചോദനമായി ഭവിച്ചു. ഏറ്റവും ബൃഹത്തായ 'ULCC' (അള്‍ട്രാ-ലാര്‍ജ് ക്രൂഡ് കാരിയര്‍) എന്നയിനം ടാങ്കറിന് ഏകദേശം 500,000 ഡെഡ് വെയ്റ്റ് ടണ്‍ ഭാരശേഷി ഉണ്ടായിരുന്നു. 

ടാങ്കറിനു ഭാരിച്ച നിര്‍മാണ ചെലവാണുള്ളത്. അപൂര്‍വം ചില സ്വകാര്യ ഏജന്‍സികള്‍ക്കു ടാങ്കറുകളുടെ ഉടമസ്ഥാവകാശം ഉണ്ടെങ്കിലും മിക്കപ്പോഴും വന്‍കിട എണ്ണ കമ്പനികള്‍ക്കായിരിക്കും ഭൂരിപക്ഷം ടാങ്കറുകളുടേയും ഉടമസ്ഥതയുള്ളത്. ഇവയ്ക്ക് നിശ്ചിത സഞ്ചാരപാതകളും കാണും. ടാങ്കര്‍ നിര്‍മാണം, പ്രവര്‍ത്തനം എന്നിവയെ വിലയിരുത്തുന്നത് ഇന്റര്‍ഗവണ്‍മെന്റല്‍ മാരിടൈം കണ്‍സല്‍റ്റേറ്റീവ് ഓര്‍ഗനൈസേഷന്‍ (IMCO) എന്ന പേരിലറിയപ്പെടുന്ന സംഘടനയാണ്. സമുദ്ര മലിനീകരണ നിയന്ത്രണം, കപ്പലോട്ട സുരക്ഷ, ആവശ്യമായ അഗ്നിശമന സൗകര്യങ്ങള്‍, നിര്‍മാണ രീതിയിലുള്ള ആഗോള സഹകരണം, സഞ്ചാരപാത നിര്‍ണയനം തുടങ്ങി ടാങ്കറുമായി ബന്ധപ്പെട്ട വിവിധ മേഖലകളെ സംബന്ധിച്ചുള്ള ഗവേഷണങ്ങളും ഈ സംഘടനയുടെ നേതൃത്വത്തില്‍ നടന്നുവരുന്നുണ്ട്.