= അടന്ത =

കഥകളി സംഗീതത്തിലെ ഒരു താളം. അടന്ത, മുറിയടന്ത, ചെമ്പ, ചെമ്പട, പഞ്ചാരി എന്നിവയാണ് പ്രധാന താളങ്ങള്‍. കര്‍ണാടകസംഗീതത്തിലെ രാഗങ്ങള്‍ക്കും താളങ്ങള്‍ക്കും കഥകളി സംഗീതത്തിലെ രാഗങ്ങളോടും താളങ്ങളോടും സാദൃശ്യമുണ്ട്. കര്‍ണാടകസംഗീതത്തിലെ ഖണ്ഡജാതി അടതാളത്തിന്റെ കേരളീയനാമമാണ് അടന്ത. കേരളീയ താളമേളങ്ങളിലെല്ലാം അടന്തക്കൂറുകള്‍ ധാരാളമായി ആലപിച്ചുവരുന്നു. താളരൂപത്തിനാണ് 'കൂറ്' എന്നു പറയുന്നത്. തായമ്പകയിലെ അടന്തക്കൂറുകള്‍ വിശേഷ പരിഗണന അര്‍ഹിക്കുന്നു.

(വി.എസ്. നമ്പൂതിരിപ്പാട്)
[[Category:കഥകളി]]