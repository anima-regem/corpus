==ആര്യശൂരന്‍==
പ്രസിദ്ധ സംസ്‌കൃത ബൗദ്ധകവി. അശ്വഘോഷകവിക്കു ശേഷമാണ്‌ ഇദ്ദേഹം ജീവിച്ചിരുന്നത്‌ എന്നു ഗവേഷകർ കരുതുന്നു. ഏതാനും ബുദ്ധാപദാനകഥകളടങ്ങുന്ന ജാതകമാല എന്ന സംസ്‌കൃതഗ്രന്ഥമാണ്‌ ഇദ്ദേഹത്തിന്റെ പ്രധാനകൃതി. എ.ഡി. 3-4 ശ.-ങ്ങളിൽ ജീവിച്ചിരുന്നു എന്നതിൽ കവിഞ്ഞ്‌ ആര്യശൂരന്റെ ജീവിതകാലത്തെ സംബന്ധിക്കുന്ന വിശ്വാസയോഗ്യമായ വിവരങ്ങളൊന്നും ലഭിച്ചിട്ടില്ല.

ജാതകമാല ഗദ്യപദ്യാങ്ങകമായ ഒരു കൃതിയാണ്‌. പാലിഭാഷയിലുള്ള ജാതകകഥകളിലും ചര്യാപിടകത്തിലും നിന്ന്‌ എടുത്ത  34 കഥകളാണ്‌ ഇതിൽ അടങ്ങിയിട്ടുള്ളത്‌. ജാതകമാലയിലെ രചനയ്‌ക്ക്‌ അനുരോധമായ പല ചിത്രശില്‌പങ്ങളും അജന്താഗുഹകളിൽ കാണാനുണ്ട്‌. ഈ ഗ്രന്ഥത്തെ ചൈനീസ്‌ ബുദ്ധമതപണ്ഡിതനായ ഇത്‌സിങ്‌ ശ്ലാഘിച്ചിട്ടുണ്ട്‌.

ബൗദ്ധന്മാരുടെ പുണ്യഗ്രന്ഥങ്ങള്‍ (Sacred Books of the Buddhists) െഎന്ന പരമ്പരയിൽ ജെ. എസ്‌. സ്‌പെയർ എന്ന പണ്ഡിതന്‍ ഇത്‌ 1895-ൽ ഇംഗ്ലീഷിലേക്ക്‌ വിവർത്തനം ചെയ്യുകയുണ്ടായി. വിശദമായ വ്യാഖ്യാനത്തോടുകൂടിയ ഇതിന്റെ ഒരു പുതിയ പ്രസാധനം ദർഭംഗയിൽനിന്ന്‌ പി.എൽ. വൈദ്യ നിർവഹിച്ചിട്ടുണ്ട്‌ (1959). ഈ കൃതിയുടെ ചീനപരിഭാഷ 10-ാം ശ.-ത്തിൽ പ്രകാശനം ചെയ്‌തതായി രേഖകളുണ്ട്‌.