==ജലമത്സരങ്ങള്‍==

ആദ്യത്തെ ജലമത്സരം (വള്ളംകളി) നടന്നത് അയ്യായിരം വര്‍ഷങ്ങള്‍ക്കു മുമ്പ് ഏഥന്‍സിലാണ്. ബാബിലോണിയ, ഗ്രീസ്, റോം തുടങ്ങിയ രാഷ്ട്രങ്ങളില്‍ പുതുവത്സര പിറവിയോടൊപ്പം വള്ളംകളികള്‍ നടത്താറുണ്ടായിരുന്നു. ഫ്രഞ്ചു ചരിത്രപണ്ഡിതന്മാര്‍ ഏഥന്‍സില്‍ നടത്തിയ ഗവേഷണത്തില്‍ കണ്ടെടുത്ത അതി പ്രാചീനമായ ഗ്രന്ഥശേഖരത്തില്‍ അയ്യായിരം വര്‍ഷം മുന്‍പ് ഏഥന്‍സില്‍ നടന്ന വള്ളംകളിയുടെ വിവരങ്ങളുണ്ട്. യുദ്ധദേവതയായ അഥീനയുടെ പ്രീതിക്കായി ഗ്രീക്കുകാര്‍ പ്രാചീന ഏഥന്‍സില്‍ പതിനാലു രാത്രിയും പതിനാലു പകലുമായി സംഘടിപ്പിച്ചിരുന്ന നവവത്സരാഘോഷങ്ങളില്‍ (ഇതു തന്നെയായിരുന്നു അവരുടെ ദേശീയ ഉത്സവവും) ഒരിനമായിരുന്നു വള്ളംകളി. മറ്റൊരു ഗ്രീക്കു ദൈവമായ മുതലയ്ക്കുവേണ്ടിയും അവര്‍ ജലോത്സവങ്ങള്‍ നടത്തിയിരുന്നു.

[[ചിത്രം:Vallamkali.png|200px|right|thumb|വള്ളംകളി]]
   
ഏഥന്‍സില്‍ നിന്നും വള്ളംകളി റോമിലെത്തിയതോടെ വെനീസിനു ചുറ്റുമായി ജലമേള കേന്ദ്രീകരിച്ചു. തണ്ടുകള്‍കൊണ്ട് ജാലവിദ്യകള്‍ കാട്ടാന്‍ റോമന്‍ തുഴക്കാര്‍ ബഹുസമര്‍ഥരായിരുന്നു. ഈ കായികകല റോമില്‍ നിന്നു ലോകമെമ്പാടും എത്തി. ബ്രിട്ടനായിരുന്നു ഇതില്‍ ഏറ്റവും മുന്‍പില്‍. അവര്‍ രാജ്യത്തുടനീളം വള്ളംകളി ക്ളബ്ബുകള്‍ രൂപീകരിച്ചു. ഓക്സ്ഫഡ്, കേംബ്രിജ് തുടങ്ങിയ സര്‍വകലാശാലകളില്‍ വള്ളംകളിക്കു വേണ്ടത്ര പ്രാധാന്യം ലഭിച്ചു. ഈറ്റണ്‍, വെസ്റ്റ്മിന്‍സ്റ്റര്‍ തുടങ്ങിയ പബ്ലിക് സ്കൂളുകളും വള്ളംകളിയില്‍ പങ്കെടുത്തിരുന്നു. ക്രമേണ ബ്രിട്ടനില്‍ വള്ളംകളി ഒരു ദേശീയ ഉത്സവമായി മാറി. 1829-ല്‍ ആയിരുന്നു ബ്രിട്ടനില്‍ ബോട്ടുക്ളബ്ബുകള്‍ ആദ്യമത്സരം സംഘടിപ്പിച്ചത്. ഓക്സ്ഫഡും കേംബ്രിജും തമ്മിലുള്ള ആദ്യമത്സരം തേംസ് നദിക്കരയിലെ ഹെന്‍ലിയില്‍ വച്ചായിരുന്നു. ആദ്യത്തെ ട്രോഫി കേംബ്രിജ് നേടി.
   
1896-ല്‍ തുടങ്ങിയ ആധുനിക ഒളിമ്പിക്സ് മത്സരത്തില്‍ 1900-ത്തില്‍ വള്ളംകളിയും ഉള്‍പ്പെടുത്തിയിരുന്നു. ഒളിമ്പിക്സിലെ വള്ളങ്ങള്‍ വീതി കുറഞ്ഞ് 8 മീ. ഓളം നീളത്തില്‍ എട്ടു പേര്‍ക്കു തുഴയാവുന്നവയാണ്.
   
സിംഗപ്പൂരിലെ 'ഡ്രാഗണ്‍ റേസ്' എന്ന ജലമേള പ്രസിദ്ധമാണ്. തായ്ലന്‍ഡിലെ ബാങ്കോക്കില്‍ പുതുവത്സരത്തോടനുബന്ധിച്ച് ജലമേള നടത്താറുണ്ട്. നോര്‍വെയിലെ വള്ളംകളിയും സ്നേക്ക് ബോട്ടും ലോകശ്രദ്ധയാകര്‍ഷിച്ചവയാണ്. ലക്ഷദ്വീപ് സമൂഹത്തില്‍ മിനിക്കോട് ദ്വീപിലെ 36 ച.കി.മീ. വിസ്തീര്‍ണമുള്ള തടാകത്തില്‍ ദ്വീപു നിവാസികള്‍ വള്ളംകളികള്‍ സംഘടിപ്പിക്കാറുണ്ട്. 'ജഹാദോണി' എന്ന ചുണ്ടന്‍വള്ളംമാതിരിയുള്ള വഞ്ചികളാണ് ഇവിടെ ജലോത്സവത്തിനായി ഉപയോഗിക്കുന്നത്. ഒരു വഞ്ചിയില്‍ 40-ല്‍പ്പരം ആളുകളുണ്ടാവും.
   
സിന്ധുനദീതട സംസ്കാരത്തിനു മുന്‍പുതന്നെ ഭാരതത്തിന് ഒരു നാവിക പാരമ്പര്യമുണ്ടായിരുന്നു. അനേകം തുഴകളുള്ള വള്ളങ്ങളെപ്പറ്റി ഋഗ്വേദത്തില്‍ പ്രതിപാദിച്ചിട്ടുണ്ട്. ബൌദ്ധജാതക കഥകളിലും വ്യാപാരക്കപ്പലുകളെക്കുറിച്ചു പറഞ്ഞിട്ടുണ്ട്.
   
ചന്ദ്രഗുപ്തമൗര്യന്‍ പാടലീപുത്രം ഭരിച്ചിരുന്ന കാലത്ത് ദക്ഷിണഭാരതത്തില്‍ ധാരാളം ഗ്രീക്കു പ്രതിനിധികള്‍ ഉണ്ടായിരുന്നതായി മെഗസ്തനീസ് രേഖപ്പെടുത്തിയിട്ടുണ്ട്.
   
അവര്‍ ദക്ഷിണ ഭാരതത്തില്‍ ജലോത്സവം അവതരിപ്പിച്ചിരിക്കാന്‍ സാധ്യതയുണ്ടെന്ന ഒരു അഭിപ്രായവും ചരിത്രപണ്ഡിതന്മാര്‍ക്കുണ്ട്.
   
പുരാതന സംസ്കൃത ഗ്രന്ഥമായ യുക്തികല്പതരുവില്‍ ഭാരതത്തിലെ കപ്പല്‍ നിര്‍മാണത്തെപ്പറ്റി വിവരിച്ചിട്ടുണ്ട്. ഭോജദേവന്റെ ഒരു ഗ്രന്ഥത്തില്‍ 27 തരം കപ്പലുകളെപ്പറ്റി പരാമര്‍ശമുണ്ട്. ഒരു ഗ്രീക്കു വ്യാപാരി രചിച്ച ചെങ്കടലിലൂടെ ഒരു പര്യടനം എന്ന പുസ്തകത്തില്‍ കേരളത്തിലെ കപ്പലുകളെക്കുറിച്ചു പരാമര്‍ശമുണ്ട്. അറബികളുടെ 'കോട്ടിയ'യില്‍ പലതുമുണ്ടാക്കിയിരുന്നത് കേരളത്തിലായിരുന്നു. കാശിക്കടുത്ത് ഗംഗാനദിയിലും മധുരയ്ക്കടുത്ത് വൈഗയിലും ജലോത്സവം നടന്നുവരുന്നു.
   
നദികളും പോഷകനദികളുംകൊണ്ട് സമ്പന്നമായ മലയാളനാട്ടില്‍ ചെറുതും വലുതുമായി നിരവധി ജലോത്സവങ്ങള്‍ നടക്കുന്നുണ്ട്. ഓണം നാളുകളിലാണ് പ്രധാനമായും ഇവ നടക്കുന്നത്. വള്ളംകളിയെ സംബന്ധിച്ച് രാജാക്കന്മാരുമായോ ക്ഷേത്രങ്ങളുമായോ ബന്ധപ്പെട്ട പല ഐതിഹ്യങ്ങളും ഉണ്ട്. കേരള സംസ്കാരത്തിന്റെ പ്രതിഫലനമാണ് വള്ളംകളിരംഗത്ത് കാണുന്നത്. ജാതിഭേദമോ ഉച്ചനീചത്വമോ ഇല്ലാതെ ഏകോദര സഹോദരങ്ങളെപ്പോലെ തുഴച്ചില്‍കാര്‍ പെരുമാറുന്നു. ഇത്രയധികം അംഗങ്ങള്‍ പങ്കെടുക്കുന്ന ഒരു മത്സരം വേറെയുണ്ടാവില്ല. ചുണ്ടന്‍ വള്ളങ്ങളില്‍ അമരക്കാര്‍, നിലക്കാര്‍ തുടങ്ങിയവരെ കൂടാതെ തുഴക്കാര്‍ മാത്രം നൂറിലധികമുണ്ടാകും. ബ്രിട്ടനിലെയും മറ്റും ജലമത്സരങ്ങള്‍ ലോകപ്രസിദ്ധമാണെങ്കിലും കേരളത്തിലെ ജലമേളകളുടെ വര്‍ണപ്പൊലിമയും ആകര്‍ഷണീയതയും ജനപങ്കാളിത്തവും ലോകത്തെ ഒരു ജലോത്സവത്തിനുമില്ല.
   
ചമ്പക്കുളം മൂലം വള്ളംകളിയാണ് കേരളത്തിലെ ജലോത്സവങ്ങള്‍ക്ക് തുടക്കം കുറിക്കുന്നത്. 1952-ലാണ് ചമ്പക്കുളം മത്സരവള്ളംകളി ആരംഭിച്ചത്. മിഥുനമാസത്തിലെ മൂലം നക്ഷത്രത്തിലാണ് ഈ വള്ളംകളി.
   
ആറന്മുള ജലമേള ക്ഷേത്രാചാരവുമായി ബന്ധപ്പെട്ടിരിക്കുന്നു. ദൃശ്യഭംഗിക്കു പ്രാധാന്യം കല്പിക്കുന്ന ഈ ജലമേളയെ വെള്ളത്തിലെ പൂരമായി വിശേഷിപ്പിക്കുന്നു. ആറന്മുള ഉതൃട്ടാതി വള്ളംകളിയോടെയാണ് കേരളത്തിലെ ജലോത്സവങ്ങള്‍ക്കു തിരശ്ശീല വീഴുന്നത്.
   
ആലപ്പുഴയ്ക്കും ഹരിപ്പാടിനുമിടയിലുള്ള പായിപ്പാട്ട് നടക്കുന്ന ജലമേളയാണ് കേരളത്തിലെ ഏറ്റവും ദൈര്‍ഘ്യമേറിയ ജലമേള. ചിങ്ങമാസം ആദ്യം ആരംഭിച്ച് ശ്രീനാരായണഗുരു ജയന്തി ദിനമായ ചതയം നാളിലാണ് പായിപ്പാട്ട് ജലോത്സവത്തിന്റെ പരിസമാപ്തി. 
   
കുമരകം ചതയം വള്ളംകളി (ശ്രീനാരായണ വള്ളംകളി) ചതയം നാളിലാണ് നടക്കുന്നത്. തൊട്ടുകൂടായ്മയ്ക്കും തീണ്ടിക്കൂടായ്മയ്ക്കും എതിരെ ശ്രീനാരായണഗുരുവിന്റെ നേതൃത്വത്തില്‍ സാമൂഹ്യപരിഷ്കരണപ്രവര്‍ത്തനങ്ങള്‍ ഊര്‍ജ്വസ്വലമായി നടന്നിരുന്ന കാലഘട്ടത്തില്‍ കുമരകത്തെ ഈഴവ പ്രമുഖര്‍ ഗുരുദേവനെ കുമരകത്തേക്കു ക്ഷണിച്ചു. മട്ടാഞ്ചേരിയില്‍ നിന്നും അനേകം കളിവള്ളങ്ങളുടെ അകമ്പടിയോടെ അദ്ദേഹത്തെ കുമരകത്തേക്കു കൊണ്ടുവന്നു. കുമാരമംഗലം എന്ന പേരില്‍ ഒരു സുബ്രഹ്മണ്യക്ഷേത്രം അദ്ദേഹം അവിടെ സ്ഥാപിച്ചു. ഇതിന്റെ ഓര്‍മയ്ക്കായാണ് ഗുരുദേവന്റെ ജന്മനാളായ ചതയദിനത്തില്‍ വള്ളംകളി സംഘടിപ്പിക്കുന്നത്.
   
കേരളത്തിലെ മത്സരപ്രധാനമായ വള്ളംകളിയില്‍ പ്രഥമസ്ഥാനം പുന്നമടക്കായലില്‍ എല്ലാവര്‍ഷവും ആഗസ്റ്റുമാസം രണ്ടാം ശനിയാഴ്ച നടക്കുന്ന 'നെഹ്റു ട്രോഫി' ജലമേളയ്ക്കാണ്. ഭാരതശില്പി പണ്ഡിറ്റ് ജവാഹര്‍ലാല്‍ നെഹ്റുവിന്റെ കുട്ടനാട് സന്ദര്‍ശനത്തിന്റെ നിത്യസ്മാരകമാണ് ഈ ജലമേള. പണ്ഡിറ്റ്ജിയുടെ കൈയൊപ്പോടുകൂടിയ വെള്ളിയില്‍ തീര്‍ത്ത ട്രോഫിയാണ് ഒന്നാം സ്ഥാനത്തെത്തുന്ന ചുണ്ടന്‍വളളത്തിനു ലഭിക്കുന്നത്. ഇന്നു ലോകത്തിലെ ഏറ്റവും വലിയ ജലമേളയായി ഇതു മാറിയിരിക്കുന്നു.
   
1957-ല്‍ നീരേറ്റുപുറം വായനശാലാ യൂണിയന്‍, ലൈബ്രറി എന്നിവയുടെ സംയുക്താഭിമുഖ്യത്തില്‍ വള്ളംകളി പ്രേമികളായ ഒരു പറ്റം ചെറുപ്പക്കാരുടെ ശ്രമഫലമായി സംഘടിപ്പിക്കപ്പെട്ട ജലമേള നീരേറ്റുപുറം വള്ളംകളിയെന്ന് അറിയപ്പെടുന്നു.
   
കുട്ടനാടിനു പുറത്തു നടക്കുന്ന പ്രമുഖ വള്ളംകളിയാണ് കണ്ടശാംകടവ് ജലമേള. ഇതില്‍ പങ്കെടുക്കുന്ന വള്ളങ്ങളെല്ലാമെത്തുന്നത് കുട്ടനാട്ടില്‍ നിന്നു തന്നെയാണ്. രണ്ടാം ഓണത്തിന് അവിട്ടം നാളിലാണ് ഈ ജലമേള നടക്കുന്നത്.
   
എല്ലാ വര്‍ഷവും സെപ്. 2-ന് കുര്യത്തുകടവില്‍ നടക്കുന്ന മാന്നാര്‍ മഹാത്മാ വള്ളംകളിയും പ്രസിദ്ധമാണ്. കോട്ടയത്ത് താഴത്തങ്ങാടി ആറ്റിലും കരുവാറ്റാ ലീഡിങ് ചാനലിലും നടക്കുന്ന (കരുവാറ്റാ വള്ളംകളി) ജലമേളകളും അനേകായിരങ്ങളെ ആകര്‍ഷിച്ചുവരുന്നു.
   
പുതുതായി സംഘടിപ്പിക്കപ്പെട്ട ചില ജലമേളകളില്‍ എറണാകുളത്തു നടക്കുന്ന ഇന്ദിരാഗാന്ധി ബോട്ട് റേസ് പ്രാധാന്യമാര്‍ജിച്ചുവരുന്നു. ഏതാണ്ട് പതിനാറോളം ചുണ്ടന്‍ വള്ളങ്ങള്‍ പങ്കെടുക്കുന്ന ഈ വള്ളംകളി മത്സരം എല്ലാവര്‍ഷവും ഡിസംബര്‍ അവസാനയാഴ്ചയാണ് നടത്തുന്നത്.
   
ആലപ്പുഴ ടൂറിസം ഡെവലപ്മെന്റ് കോ-ഓപ്പറേറ്റീവ് സൊസൈറ്റി ഓരോ വര്‍ഷവും ജൂലായ് മാസത്തിലെ മൂന്നാം ശനിയാഴ്ച ആലപ്പുഴയില്‍ വള്ളംകളി സംഘടിപ്പിക്കാറുണ്ട്.
   
നാല്പതിലേറെ ചുണ്ടന്‍വള്ളങ്ങള്‍ പങ്കെടുക്കുന്ന മറ്റൊരു ജലമേളയാണ് ആലപ്പുഴ നിന്നു മൂന്നു കി.മീ. മാറി പുളിങ്കുന്നില്‍ സംഘടിപ്പിക്കുന്ന രാജീവ് ഗാന്ധി ട്രോഫി ബോട്ട് റേസ്.
   
തിരുവനന്തപുരത്തെ ശുദ്ധജല തടാകമായ വെള്ളായണി കായലില്‍ ചതയം നാളില്‍ സാധാരണ വള്ളങ്ങളുടെ മത്സരം നടത്തുന്നു.
   
കേരളത്തിലെ ക്ഷേത്രക്കുളങ്ങളില്‍ ഏറ്റവും വലിയ രണ്ടാമത്തെ കുളമായ ശ്രീവരാഹം (തിരുവനന്തപുരം) കുളത്തിലും ചെറിയ വള്ളങ്ങളുടെ മത്സരം ഓണത്തോടനുബന്ധിച്ചു നടത്താറുണ്ട്.

(രമേശ് ബാബു)