==കൊളോഡിയോണ്‍==

==Collodion==

ആള്‍ക്കഹോള്‍-ഈഥര്‍ മിശ്രിതത്തില്‍ വെടിപ്പഞ്ഞിയോ (gun cotton) മറ്റ് ഏതെങ്കിലും പൈറോക്സിലിനോ ലയിപ്പിച്ച് തയ്യാറാക്കുന്ന നിറമില്ലാത്തതും ഒട്ടിപ്പിടിപ്പിക്കുന്നതുമായ ഒരു ദ്രവം. കൊളോഡിയോണിന്റെ ഗുണമേന്മ ആല്‍ക്കഹോള്‍-ഈഥര്‍ അനുപാതത്തെയും ലയിപ്പിച്ച പൈറോക്സിലിന്റെ സ്വഭാവത്തെയും ആശ്രയിച്ചിരിക്കുന്നു. ഈഥര്‍ അധികമുള്ള കൊളോഡിയോണ്‍ ബാഷിപീകരിച്ചാല്‍ കട്ടിയുള്ള ഫിലിം ഉണ്ടാകുന്നു. ആള്‍ക്കഹോള്‍ അധികമുള്ള കൊളോഡിയോണ്‍ ബാഷ്പീകരിച്ചുണ്ടാകുന്ന ഫിലിം മൃദുവും പെട്ടെന്ന്  പൊട്ടിപ്പോകുന്നതുമായിരിക്കും. ശുദ്ധമായ കൊളോഡിയോണ്‍ ഫിലിം നിറമില്ലാത്തതും അര്‍ധതാര്യവും (translucent) ആണ്. തണുപ്പുള്ള സ്ഥലത്താണ് കൊളോഡിയോണ്‍ സൂക്ഷിക്കണ്ടത്; പ്രകാശവുമായി സമ്പര്‍ക്കപ്പെടാന്‍ ഇതിനെ അനുവദിക്കയുമരുത്. കൊളോഡിയോണിനെ ഫലപ്രദമായി അയോഡീകരിക്കാം. 

കൊളോഡിയോണ്‍ ത്വക്കില്‍ പുരട്ടിയാല്‍ അതു വേഗത്തില്‍ ഉണങ്ങി ചുരുങ്ങുകയും മര്‍ദം സഹിക്കുന്ന ഒരു ഫിലിം ഉണ്ടാകുകയും ചെയ്യുന്നു. അതിനാല്‍ പുരാതനകാലം മുതല്ക്കേ ഇത് ശസ്ത്രക്രിയയ്ക്ക് ഉപയോഗിച്ചു വരുന്നു. ആവണക്കെണ്ണ, കാനഡ ബാള്‍സം എന്നിവ അടങ്ങിയ ഒരു തരം കൊളോഡിയോണ്‍ ഉണ്ട്. ഇത് വഴക്കമുള്ളതാണ്; പൊട്ടുകയുമില്ല; സാധാരണ കൊളോഡിയോണിനെപ്പോലെ ചുരുങ്ങുകയുമില്ല, അര്‍ധതാര്യസ്തരമെന്ന നിലയില്‍ കൊളോഡിയോണ്‍ ഫിലിം ഓസ്മോസനവുമായി ബന്ധപ്പെട്ട പരീക്ഷണങ്ങള്‍ക്കും മറ്റും ഉപയോഗിച്ചുവരുന്നു.

(ചുനക്കര ഗോപാലകൃഷ്ണന്‍)