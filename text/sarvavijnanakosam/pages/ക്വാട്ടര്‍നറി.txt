==ക്വാട്ടര്‍നറി==

ഭൂവിജ്ഞാനീയ സമയക്രമത്തിലെ സീനസോയിക് യുഗത്തിലെ അവസാനത്തെ കാലഘട്ടം. ഇന്റര്‍നാഷണല്‍ കമ്മിഷന്‍ ഒണ്‍ സ്രാറ്റിറാഗ്രഫി ((International Commission on Stratyraphy -ICS)ആണ് ഈ പദം ആദ്യമായി സന്നിവേശിപ്പിച്ചത്. ജിയോവനി അര്‍ഡുയിനോ എന്ന ശാസ്ത്രജ്ഞന്‍ ഇറ്റലിയിലെ പോ നദീതടത്തിലെ എക്കലുകള്‍ക്ക് 1759-ല്‍ ക്വാട്ടര്‍നറി എന്ന പേരു നല്‍കി. തുടര്‍ന്ന് 1829-ല്‍ ജെ. ഡെസ്നോയേഴ്സ് എന്ന ഭൂവിജ്ഞാനി ഫ്രാന്‍സിലെ സീന്‍ നദീതടത്തിലെ അവസാദങ്ങളെ പ്രതിപാദിക്കുവാനും ഈ പദം ഉപയോഗിക്കുകയുണ്ടായി.
   
ഐ. സി. എസ്സിന്റെ നിര്‍വചനപ്രകാരം (2009) ക്വാട്ടര്‍നറിയുടെ കാലദൈര്‍ഘ്യം 2.588 ദശലക്ഷം വര്‍ഷം മുതല്‍ ഇപ്പോള്‍ വരെയുള്ള കാലഘട്ടമാണ്. ക്വാട്ടര്‍നറി കല്പത്തിന് 2 വിഭാഗങ്ങളുണ്ട് - പ്ലീസ്റ്റസീനും റീസന്റും. ഇതില്‍ പ്രായം കൂടിയ പ്ലീസ്റ്റസീന്‍ യുഗം 2.588 ദശലക്ഷം വര്‍ഷങ്ങള്‍ക്കു മുമ്പ് ആരംഭിച്ച് 11700 വര്‍ഷങ്ങള്‍ക്കു മുമ്പ് അവസാനിച്ചു. പ്ലീറ്റസീന്‍ യുഗത്തില്‍ വടക്കേ അമേരിക്കയും ഉത്തരയൂറോപ്പും പലപ്രാവശ്യം ഹിമനദീയനങ്ങള്‍ക്ക് വിധേയമായി. ഇതേകാലഘട്ടത്തിലായിരുന്നു മനുഷ്യന്റെ ആവിര്‍ഭാവം.
   
ക്വാട്ടര്‍നറി കല്പത്തിലെ രണ്ടാമത്തെ വിഭാഗമായ റീസന്റ്യുഗം ഹോളോസീന്‍ എന്ന പേരിലും അറിയപ്പെടുന്നു. 11700 വര്‍ഷങ്ങള്‍ക്കുമുമ്പ് ആരംഭിച്ച റീസന്റ് വര്‍ത്തമാനകാലഘട്ടത്തെക്കൂടി പ്രതിനിധാനം ചെയ്യുന്നു.
   
പ്ലീസ്റ്റസീന്‍ കാലഘട്ടത്തില്‍ ഭൂമിയിലെ മിക്ക പ്രദേശങ്ങളും ഹിമപാളികള്‍ക്കടിയിലായിരുന്നു. എന്നാല്‍ ഇടയ്ക്ക് താപനിലവര്‍ധിച്ചതിന്റെ ഫലമായി മഞ്ഞ് ഉരുകിയതോടെ ഹിമനദികള്‍ ഉദ്ഭവിച്ചു തുടങ്ങി. തത്സമയം സമുദ്രനിരപ്പും ഉയര്‍ന്നിരുന്നതായി തെളിവുണ്ട്. ഹിമാലയ പര്‍വതങ്ങളുടെ ഉത്ഥാനത്തിന്റെ അവസാനഘട്ടം ക്വാട്ടര്‍നറി കാലഘട്ടത്തിന്റെ അവസാനത്തോടെയായിരുന്നു. ഇന്ത്യയുടെയും മ്യാന്മറിന്റെയും ഭൂരിഭാഗം പ്രദേശങ്ങളും ഈ യുഗത്തില്‍ കരയായിരുന്നു. എന്നാല്‍ റാന്‍ ഒഫ് കച്ച്, ഥാര്‍ മരുഭൂമി തുടങ്ങിയ പ്രദേശങ്ങള്‍ സമുദ്രത്തിനടിയിലായിരുന്നു.
   
ക്വാട്ടര്‍നറി കല്പത്തില്‍ പലപ്രാവശ്യം സമുദ്രനിരപ്പ് ഉയരുകയും താഴുകയും ചെയ്തിരുന്നു. ഈ കാലയളവില്‍ പല വലിയ സസ്തനജീവികളുടെയും വംശനാശം സംഭവിച്ചു. വടക്കേ അമേരിക്കയില്‍ കുതിര, ഒട്ടകം, അമേരിക്കന്‍ ചീറ്റാ എന്നിവ പോലുള്ളവ എന്നന്നേക്കുമായി അപ്രത്യക്ഷമായി. മാമത്ത് എന്നറിയപ്പെടുന്ന പ്രാചീനഗജം (ആനയുടെ പൂര്‍വികര്‍), മാസ്റ്റോഡോണ്ടുകള്‍, ഗ്ലിപ്റ്റോഡോണ്ടുകള്‍ പോലുള്ളവ ഭൂമുഖത്തുനിന്നുതന്നെ അപ്രത്യക്ഷമായി. ക്വാട്ടര്‍നറി കല്പത്തില്‍ ഉണ്ടായ വലിയ പാരിസ്ഥിതിക മാറ്റങ്ങള്‍മൂലം ഭൂപ്രകൃതിയിലും മനുഷ്യന്റെ ജീവിതശൈലിയിലും ധാരാളം വ്യതിയാനങ്ങള്‍ സംഭവിച്ചു.
   
ഇന്ത്യയില്‍ ഹിമാലയത്തിലെ സിവാലിക് പ്രദേശങ്ങളില്‍ അധോ പ്ളീസ്റ്റോസീന്‍ നിക്ഷേപങ്ങള്‍ കാണപ്പെടുന്നുണ്ട്. ഈ നിക്ഷേപങ്ങളില്‍ കാണപ്പെടുന്ന കാള, കുതിര, ഒട്ടകം എന്നിവയുടെ പൂര്‍വിക ഫോസിലുകള്‍ ലോകപ്രസിദ്ധമാണ്. ആന്ത്രപ്പോയിഡ് കുരങ്ങുകളുടെ അവശിഷ്ടങ്ങളും ഇവിടെയുള്ള അവസാദങ്ങളില്‍ നിന്ന് ലഭിച്ചിട്ടുണ്ട്.

(ഡോ. എസ്.എന്‍.കുമാര്‍; സ.പ.)