കുന്നിനു മുകളിലേയ്ക്കുള്ള ഒറ്റയടിപ്പാതയുടെ തുടക്കത്തിൽ അയാൾ കാർ നിർത്തി. ഇനി മുകളിലേയ്ക്കു് കാർ പോവില്ല. ചെമ്മൺപാത ഒരു ധൃതിയുമില്ലാതെ മുകളിലേയ്ക്കു് ഇഴഞ്ഞു നീങ്ങുന്നതു് മോഹനൻ നോക്കി. രാജീവനും ചുറ്റും നോക്കുകയായിരുന്നു. അവന്റെ കണ്ണുകളിൽ കൗതുകം. ഒരുപക്ഷേ, ഇങ്ങിനെയൊരു സ്ഥലത്തു വരുന്നതു് അവൻ ആദ്യമായിട്ടായിരിക്കണം. മുത്തശ്ശിവല്ല്യമ്മയുടെ വീട്ടിൽ പോകാമെന്നു പറഞ്ഞപ്പോൾ അവൻ ഇത്രയൊന്നും പ്രതീക്ഷിച്ചിരിക്കില്ല. സാധാരണ നാട്ടിൽ വരുമ്പോൾ അച്ഛമ്മയെ കാണാറുണ്ടു്. അച്ഛമ്മ താമസിക്കുന്ന പഴയ വീടു് അവനു് ഇഷ്ടമാകാറുണ്ടു്. അതൊരു വെറും നാട്ടിൻപുറം മാത്രമാണു്. ഋതുമതിയാകാൻ പോകുന്ന ഒരു കുട്ടിയുടെ മട്ടിൽ അതൊതുങ്ങിനിൽക്കയാണു്. അച്ഛമ്മയുടെ ചെറിയമ്മയുടെ വീട്ടിലേയ്ക്കാണു് ഇപ്പോൾ പോകുന്നതു്. അവൻ ഇതുവരെ കണ്ടിട്ടില്ലാത്ത മുത്തശ്ശിവല്ല്യമ്മ.
‘ഇനി നമുക്കു് നടക്കണം.’
‘ശരി’ രാജീവൻ ഉത്സാഹത്തോടെ പറഞ്ഞുകൊണ്ടു് നടക്കാൻ തുടങ്ങി.
തലേന്നു പെയ്ത മഴയിൽ മണ്ണു നനഞ്ഞിരുന്നു. ചെടികളുടെ ഇലകൾ കഴുകപ്പെട്ടിരുന്നു. കാറ്റിൽ ഏതോ പൂക്കളുടെ, ഇലകളുടെ സുഗന്ധം. ഈ ഗന്ധങ്ങൾ പണ്ടെങ്ങോ തനിക്കു നഷ്ടപ്പെട്ട കുട്ടിക്കാലം തിരിച്ചു നൽകുകയാണു്. അയാൾ വീണ്ടും ഒരു കുട്ടിയായി, മീനാക്ഷിവല്ല്യമ്മയുടെ കുട്ടനായി.
ഒരു തിരിവിൽ തെളിവെള്ളം നിറഞ്ഞ ഒരു ചെറിയ അരുവി പാതയുടെ ഒപ്പം ചേർന്നു. അയാൾ കൗതുകത്തോടെ നോക്കി. രാജീവനും അതുതന്നെയായിരുന്നു നോക്കിയിരുന്നതു്. മദ്രാസിൽ ഇങ്ങിനെയുള്ള തെളിവെള്ളം വീട്ടിലെ ടാപ്പിൽ മാത്രമേ കാണൂ. നിരത്തുകളുടെ അരുകിലുള്ള ഓടകളിൽ കെട്ടിനിൽക്കുന്ന അഴുക്കുവെള്ളം മാത്രം, പുഴയും ഈ ഓടകളെ സ്പോൺസർ ചെയ്യുകവഴി മലിനമാണു്. ഇവിടെ ഇതാ ഒരരുവി, മനുഷ്യസ്പർശമേറ്റിട്ടുണ്ടെന്നു തോന്നാത്ത വിധത്തിൽ തെളിവെള്ളവുമായി ഒഴുകുന്നു.
‘അച്ഛാ, ഇതെവിടെനിന്നാണു് വരണതു?’
‘കുന്നിന്റെ മുകളിൽനിന്നു്. മഴക്കാലത്തു് കുന്നിന്റെ ഉളളിലേയ്ക്കു് കിനിഞ്ഞിറങ്ങിയ വെള്ളം കുറേശ്ശെ പുറത്തു വര്വാണു്. ഒരു ഒക്ടോബർ നവമ്പർ വരെണ്ടാവും. അതുകഴിഞ്ഞാൽ ഈ അരുവി വറ്റിവരളും.’
‘പിന്നെ വെള്ളംണ്ടാവില്ല്യേ?’
‘ഇല്ല.’
‘അപ്പോ ഈ അരുവി എവിടെപ്പോകും.’
രാജീവിന്റെ ചോദ്യം അർത്ഥവത്താണു്. തന്റെ കുട്ടിക്കാലത്തു് താൻ ഇങ്ങിനെയൊരു ചോദ്യം ചോദിച്ചിട്ടില്ല. മഴയോടുകൂടി അരുവിയുടെ ഉദ്ഭവവും, മഴകഴിഞ്ഞു് മാസങ്ങൾക്കകമുള്ള തിരോധാനവും സ്വീകരിക്കപ്പെട്ട യാഥാർത്ഥ്യങ്ങളാണു്. അതു് ചോദ്യത്തിനുള്ള വക തരുന്നില്ല. മറിച്ചു് ജീവിതത്തിൽ നടാടെ ഒരരുവി കാണുകയാണു് രാജീവൻ. ഇതിനുമുമ്പു് വല്ല്യമ്മയുടെ വീട്ടിൽ പത്മയുടെ ഒപ്പം വന്നപ്പോൾ അവൻ കൈകുഞ്ഞായിരുന്നു. മദ്രാസിൽ ഒരുഫ്ളാറ്റിൽ പാഠപുസ്തകങ്ങൾക്കും, വീഡിയോഗേയ്മിനും ടിവിയുടെ മയക്കുന്ന ദൂഷ്യവലയത്തിനുമുള്ളിൽ അവന്റെ ജീവിതം കെട്ടിയിടപ്പെട്ടിരിക്കുന്നു. നാട്ടിൻപുറവും അവിടെ കെടാറായി കിടക്കുന്ന നന്മയുടെ ചൂടുതരുന്ന നെരിപ്പോടുകളും അവന്നു് അപരിചിതമാണു്.
‘ഈ അരുവി അപ്പോ എവിടെപ്പോവും അച്ഛാ?’ അവൻ ചോദ്യം ആവർത്തിക്കുന്നു.
അയാൾ ആലോചിച്ചു. ഒരുപക്ഷേ, മണ്ണിന്നടിയിൽ, ആഴത്തിൽ അതൊഴുകുന്നുണ്ടാവും. അല്ലെങ്കിൽ? ഒരരുവിയുടെ നിലനില്പു് അതിലെ വെള്ളത്തെ ആശ്രയിച്ചാണിരിക്കുന്നതു്. അപ്പോൾ വെള്ളമില്ലാതായാൽ? ഒരുപാടു് സങ്കീർണ്ണതകൾ നിറഞ്ഞതാണു് അവന്റെ ചോദ്യം.
‘അതാ, ആ കാണുന്നതാണു് വല്ല്യമ്മയുടെ വീടു്.’ അയാൾ ചൂണ്ടിക്കാട്ടി. ഒരു നാൽപതു വാര അകലെ മരങ്ങൾക്കിടയിൽ ഒരു ഓടുമേഞ്ഞ വീടു്. നാട്ടിൻപുറത്തെ മറ്റേതു വീടുംപോലെ സാധാരണമായ ഒരു വീടു്.
രാജീവു് അല്പം നിരാശനായെന്നു തോന്നുന്നു. അവൻ കൂടുതലെന്തെങ്കിലും പ്രതീക്ഷിച്ചു കാണും. കൂടുതൽ പ്രതീക്ഷിക്കുക, പിന്നെ അതു ലഭിക്കാഞ്ഞാൽ നിരാശപ്പെടുക എന്നതു് അവന്റെ സ്വഭാവമാണു്.
കാവിതേച്ച തൂണുകളിൽ തൂങ്ങിക്കിടക്കുന്ന ഗെയ്റ്റ് കടന്നു് അവർ മുറ്റത്തേയ്ക്കുള്ള വഴിയിലെത്തി. ഇരുവശത്തും കൃഷ്ണക്കിരീടം പൂത്തു നിൽക്കുന്നു. പല വലുപ്പത്തിലുള്ളവ. ഒരു രാജകുടുംബത്തിന്റെ എഴുന്നള്ളത്തുപോലെ തോന്നി.
ഉമ്മറത്തെ കറുത്ത സിമന്റിട്ട നിലം മിനുത്തു കിടന്നു. അകത്തേയ്ക്കുള്ള വാതിൽ തുറന്നു കിടക്കയാണു്. വാതിൽക്കൽ മുട്ടാനൊന്നും മിനക്കെടാതെ അയാൾ അകത്തേയ്ക്കു കടന്നു. പിന്നാലെ അല്പം മടിച്ചിട്ടാണെങ്കിലും ഒരു ചോദ്യഭാവത്തോടെ രാജീവും. എന്താണു് വാതിൽക്കൽ മുട്ടാതിരുന്നതു് എന്നാണവന്റെ ചോദിക്കാത്ത ചോദ്യം. നഗരത്തിലെ സാമാന്യമര്യാദകളിലുള്ള ഔപചാരികത്വം ഈ നാട്ടിൻപുറത്തു് മുഴച്ചുനിൽക്കും. ഇടനാഴികയുടെ ഇടതുവശത്തുള്ള രണ്ടു മുറിയിലും വല്ല്യമ്മയുണ്ടായിരുന്നില്ല. അയാൾ അവിടെയൊന്നും അവരെ പ്രതീക്ഷിച്ചതുമില്ല. ഇടനാഴിക അവസാനിക്കുന്നതു് തളത്തിലാണു്. തളത്തിൽനിന്നു് അടുക്കളയിലേയ്ക്കും അടുക്കളമുറ്റത്തേയ്ക്കും വാതിലുകളുണ്ടു്. പെട്ടെന്നു് അടുക്കളവാതിൽ കടന്നുവന്ന വല്ല്യമ്മയുടെ മുഖത്തു് അദ്ഭുതമൊന്നും കണ്ടില്ല. നാട്ടിൻപുറത്തു് ഇതു സാധാരണമാണു്. വേണ്ടപ്പെട്ടവർ തുറന്നുകിടക്കുന്ന വാതിൽ കടന്നുവരും, വല്ല്യമ്മ എവിടെയാണെന്നു പരതും.
‘രണ്ടു കൊള്ളക്കാരാണു് വന്നിരിക്കുന്നതു്.’ മോഹനൻ പറഞ്ഞു.
വല്ല്യമ്മയ്ക്കു് ആളെ മനസ്സിലായി.
‘അതു നന്നായി, ഇവിടൊന്നും സാധാരണ കാണാത്തവരാണു് കൊള്ളക്കാരു്. രണ്ടുപേരെ കാണാൻ പറ്റീലോ.’
അവർ അടുത്തുവന്നു, രാജീവിന്റെ തലയുഴിഞ്ഞുകൊണ്ടു് തുടർന്നു. ‘ഈ കൊച്ചുകൊള്ളക്കാരനെ, പ്രത്യേകിച്ചും.’
ഒരു കൊള്ളക്കാരനെന്ന സങ്കല്പം രാജീവനു് ഇഷ്ടമായെന്നു തോന്നുന്നു. അവൻ ചിരിച്ചുകൊണ്ടു് മുത്തശ്ശിവല്ല്യമ്മയുടെ ലാളനത്തിനു് വഴങ്ങിക്കൊടുത്തു.
‘വല്ല്യമ്മ എന്താ ചെയ്തിരുന്നതു?’ അയാൾ ചോദിച്ചു.
‘ഉച്ചക്കിലേയ്ക്കുള്ള ചോറുണ്ടാക്ക്വാണു്.’ അവർ അടുക്കളയിലേയ്ക്കു നടന്നുകൊണ്ടു് പറഞ്ഞു. ‘നിങ്ങളിരിക്ക്. ഞാൻ കുറച്ചു ചായയുണ്ടാക്കിക്കൊണ്ടുവരാം.’
‘ചായയ്ക്കൊന്നും ധൃതിയില്ല.’ അയാൾ അവരുടെ ഒപ്പം അടുക്കളയിലേയ്ക്കു നടന്നു. രണ്ടടുപ്പുകളിൽ വിറകു കത്തുന്നുണ്ടു്. പാത്രത്തിൽ അരിയിടാനുള്ള വെള്ളം തിളക്കുന്നു.
‘വല്ല്യമ്മയ്ക്കു് ഒരു ഗ്യാസടുപ്പു് വാങ്ങിക്കൂടെ? കുക്കിങ്ങൊക്കെ എത്ര എളുപ്പാവും?’
‘അപ്പോ ഈ പറമ്പിലു് വീഴണ മടലും, ഓലക്കൊടീം, ചുള്ളിം ഒക്കെ എന്തു ചെയ്യാനാ? പിന്നെ അടുപ്പിലു് വേവിക്കണ ഭക്ഷണത്തിന്റെ സ്വാദൊന്നും ഗ്യാസടുപ്പിൽ വേവിച്ചാൽ കിട്ടില്ല.’
‘എന്താണു് കൂട്ടാൻ?’
‘വെണ്ടക്കയും വഴുതിനങ്ങീം കൂട്ടീട്ടു് ഒരു സാമ്പാറു്. കുരുത്തോലപ്പയറും കായേംകൊണ്ടു് ഒരു മെഴുക്കു പെരട്ടീം. നിങ്ങള് വരണ കാര്യം ഞാൻ അറിഞ്ഞിരുന്നെങ്കിലു് ജാനകിയെക്കൊണ്ടു് നല്ല പച്ചക്കറിയെന്തെങ്കിലും വാങ്ങിച്ചേനെ?’
‘ആരാണു് ജാനകി?’
‘ങാ, നീയറിയില്ലേ? ഇല്ല്യ? നീ മുമ്പു വന്നപ്പോ മാലത്യായിരുന്നു. അവള് കല്യാണം കഴിച്ചു പോയി. ഇതു് അതിന്റെ അനിയത്ത്യാ. രാവിലെ വന്നു് എന്നെ സഹായിക്കും. വൈകുന്നേരവും വരും. ചെലപ്പോ എനിക്കു് വയ്യ്യാന്നു് തോന്ന്യാൽ ഞാൻ അവളോടു് ഇവിടെ കെടക്കാൻ പറയും. നല്ല കൂട്ടത്തിലാ.’
അയാൾ അടുക്കളവാതിലിലൂടെ പുറത്തേയ്ക്കു നോക്കി. വല്ല്യമ്മയുടെ തോട്ടത്തിന്റെ ഒരു കഷ്ണം വാതിലിലൂടെ കാണാം. ആരോഗ്യമുള്ള നീണ്ട വെണ്ടക്ക, പച്ചമുളക്, അതിനുമപ്പുറത്തു് ഒരു തടം ചീര. അയാൾ മുറ്റത്തേയ്ക്കിറങ്ങി.
‘ഞാൻ വല്ല്യമ്മടെ തോട്ടം കാണട്ടെ.’
രാജീവൻ പറമ്പിലെത്തിക്കഴിഞ്ഞിരുന്നു. അഴിച്ചവിട്ട ഒരു പശുക്കുട്ടിയെപ്പോലെ അവൻ ഓടിനടക്കുകയാണു്.
‘തോട്ടംന്നു് പറയാൻമാത്രൊന്നുംല്ല്യ.’ അവർ ഒപ്പം വന്നു.
തോട്ടം വിപുലംതന്നെയായിരുന്നു. ഒരു അവരപ്പന്തൽ നിറയെ കായ്ചു നിൽക്കുന്നു. അപ്പുറത്തു് മരക്കൊമ്പുകൾ ഞാത്തുകൊടുത്തതിൽ വെള്ളയും വൈലറ്റും നിറത്തിൽ കുരുത്തോലപ്പയർ നിറയെ നീണ്ടുകിടക്കുന്നു. പെട്ടെന്നയാൾക്കു് വിശപ്പുതോന്നി. വീട്ടിൽ നട്ടുണ്ടാക്കിയ പച്ചക്കറികളുടെ സ്വാദു് ഓർമ്മ വന്നു. ആ സ്വാദു് കുട്ടിക്കാലത്തേയ്ക്കു നയിക്കുന്നു. മദ്രാസിൽ കിട്ടുന്ന പച്ചക്കറികളുടെ സ്വാദെന്തണെന്നു് അയാൾക്കു് മനസ്സിലാവാറില്ല. മാർക്കറ്റിൽ നിന്നു വാങ്ങുന്ന ചീര കഴിച്ചാൽ വയറിളക്കം തീർച്ചയാണു്. ഫാക്ടറിയിൽ പോകുമ്പോൾ ഇരുവശത്തും ചീരത്തോട്ടങ്ങളിയേല്ക്കു് കാനയിലെ വൃത്തികെട്ട വെള്ളം പമ്പുചെയ്യുന്നതു് നിത്യകാഴ്ചയാണു്.
ചായകുടിക്കുമ്പോൾ അയാൾ ചോദിച്ചു.
‘വല്ല്യമ്മ എന്തിനാ ഇവിടെ ഒറ്റയ്ക്കു് താമസിച്ചു് കഷ്ടപ്പെടണതു? കൃഷ്ണൻകുട്ടീടെ ഒപ്പം പോയി താമസിച്ചൂടെ?’
‘ആർക്കാ അതിനു് ഇവടെ കഷ്ടപ്പാടു്?’ അവർ ചിരിച്ചുകൊണ്ടു് ചോദിച്ചു. ‘നല്ല കാര്യായി. ഇവിടെ എനിക്കു് സുഖല്ലേ? ഈ വയസ്സുകാലത്തു് ഞാനെന്തിനാണു് മദ്രാസിലൊക്കെ പോയി താമസിക്കണതു്. ഇവിടെ ഒരു വെഷമും ഇല്ല്യ. പിന്നെ മദ്രാസിലൊക്കെ ഞാൻ പോയിട്ട്ള്ളതല്ലെ. എനിക്കു് ആ ജീവിതം ഒന്നും ഇഷ്ടല്ല, പറയാലോ.’
ശരിയാണെന്നു് അയാൾ ഓർത്തു. നാട്ടിലേയ്ക്കു വരുമ്പോൾ കൃഷ്ണൻകുട്ടിയും ഒപ്പമുണ്ടായിരുന്നു. ഓണം അമ്മമ്മയുടെ ഒപ്പം ആവണമെന്നവനു് നിർബ്ബന്ധമുണ്ടു്. തിരിച്ചുപോകുമ്പോൾ വീട്ടിൽ വന്നപ്പോൾ അവൻ പറഞ്ഞു. ‘ഞാൻ ഒരിക്കൽ കൂടി അമ്മമ്മടെ അടുത്തു് തോറ്റിരിക്കുണു. ഇനി മോഹനേട്ടൻ ഇപ്രാവശ്യം ഒന്നവിടെ പോണം. എങ്ങിനെയെങ്കിലും അമ്മമ്മയെ പറഞ്ഞു മനസ്സിലാക്കി സമ്മതിപ്പിക്കണം. അവിടെ ഒറ്റയ്ക്കു് ഒരു സഹായവും ഇല്ലാതെ’
‘ഞാൻ ചായണ്ടാക്കട്ടെ. മോനെന്താണു് കുടിക്ക്യാ. മദ്രാസിലെപ്പോലെ ബോൺവിറ്റയൊന്നും ഇവിടെണ്ടാവില്ല.’
‘വല്ല്യമ്മ മിണ്ടാതിരി, അവൻ ചായ കുടിച്ചുകൊള്ളും.’
വല്ല്യമ്മ അടുക്കളയിലേയ്ക്കു പോയി. അയാൾ പറമ്പിലേയ്ക്കു നടന്നു.
‘അച്ഛാ’, രാജീവൻ വിളിച്ചുപറഞ്ഞു, ‘ഒരു പാമ്പു്.’
അയാൾ അടുത്തുചെന്നു. പുല്ലുകൾക്കിടയിൽ ഒരു പാമ്പു്.
‘അതു് മൂർഖൻ പാമ്പാണു്.’
‘അതു് വെഷള്ളതാണോ അച്ഛാ?’
‘അതെ, നീയിങ്ങു വാ.’
അവനെ സംബന്ധിച്ചേടത്തോളം മൂർഖൻ പാമ്പും നീർക്കോലിയും തമ്മിൽ വലിയ വ്യത്യാസമൊന്നുമില്ല. രണ്ടും പാമ്പുതന്നെ.
കുളത്തിൽ നാലു് ആമ്പൽ പൂക്കൾ വിരിഞ്ഞു നിന്നിരുന്നു. തെളിവെള്ളം. അടുത്തുചെന്നാൽ നീന്തിക്കളിക്കുന്ന മീനുകളെ കാണാൻ പറ്റും.
‘ചായ കാലായി, വരൂ.’ വല്ല്യമ്മ വിളിച്ചു പറഞ്ഞു.
അടുക്കളയിൽ ഇപ്പോൾ പത്തുപതിനെട്ടു വയസ്സുള്ള ഒരു പെൺകുട്ടി ജോലിയെടുത്തിരുന്നു.
‘ഇതാണോ വല്ല്യമ്മയുടെ അസിസ്റ്റന്റ്?’
‘ഞാൻ മോഹനനോടു് പറയ്യായിരുന്നു.’ വല്ല്യമ്മ ആ കുട്ടിയോടു് പറഞ്ഞു. ‘മാലതി കല്യാണം കഴിച്ചു് പോയപ്പോ നീ എന്നെ സഹായിക്കാൻ വരണ്ണ്ട്ന്നു്. മാലതീനെ മോഹനൻ കണ്ടിട്ട്ണ്ടു്.’
‘ചേച്ചി ഇപ്പൊ എവിട്യാണു്?’ അയാൾ ചോദിച്ചു.
‘ചേച്ചി പ്രസവിക്കാൻ വന്നിട്ട്ണ്ടു്. ഈ ധനുവിലാണു് പ്രസവം.’ അവൾ ഉത്സാഹത്തോടെ പറഞ്ഞു.
അയാൾ എന്തെങ്കിലും ചോദിക്കണ്ടേ എന്നു കരുതി ചോദിച്ചതായിരുന്നു. പക്ഷേ, അവളതു് ഗൗരവമായി എടുത്തു. അവൾ ചേച്ചിയെപ്പറ്റി പറയുകയായിരുന്നു. അവരുടെ ഭർത്താവിനെപ്പറ്റി, തേയിലത്തോട്ടത്തിൽ ജോലിചെയ്യുന്ന ഭർത്താവു് വരുമ്പോൾ ചേച്ചിക്കു കൊണ്ടുവരുന്ന സാധനങ്ങളെപ്പറ്റി. അയാളുടെ മനസ്സു് ആർദ്രമായി. ഇവിടെ അന്തരീക്ഷംപോലെത്തന്നെ മനുഷ്യരുടെ മനസ്സും തെളിഞ്ഞു് നിർമ്മലമായിരുന്നു. നന്നാവട്ടെ കുട്ടി. അയാൾ മനസ്സിൽ പറഞ്ഞു. നിനക്കും ഒരു നല്ല ഭർത്താവിനെ കിട്ടട്ടെ.
അയാൾ ഉമ്മറമുറ്റത്തേക്കിറങ്ങി. രാജീവൻ തൊടിയിലെ തൈമാവിൽ കയറാനുള്ള ശ്രമമാണു്. തന്റെ കുട്ടിക്കാലത്തു് എത്രയും അനായാസം ചെയ്തിരുന്ന ഒരു കാര്യം അവൻ വളരെ ക്ലേശിച്ചാണു് ചെയ്യുന്നതു്. അയാൾക്കു് പെട്ടെന്നു് പേടിതോന്നി. ചെറിയ മാവാണു്, കൊമ്പുകൾ അധികം ഉയരമൊന്നുമില്ല, എങ്കിലും അവന്റെ കാൽവെപ്പിൽ ഒരു ഉറപ്പില്ലായ്മ അയാൾ അസ്വസ്ഥതയോടെ നോക്കി. അവൻ കയറുന്ന ആദ്യത്തെ മരമായിരിക്കണം ഇതു്.
‘സൂക്ഷിച്ചു കയറണം കേട്ടോ.’ അയാൾ വിളിച്ചുപറഞ്ഞു. പറമ്പിന്റെ അറ്റത്തെത്തിയപ്പോൾ കുന്നു് താഴോട്ടിറങ്ങുന്നതു് കണ്ടു. കാർ നിർത്തിയിടം അധികം ദൂരമൊന്നുമില്ല. കുട്ടിക്കാലത്തു് ഇവിടെ വന്നിരുന്നപ്പോൾ ഇതു് വലിയൊരു ദൂരമായി തോന്നിയിരുന്നു.
അയാൾ കൃഷ്ണൻകുട്ടിയെ ഓർത്തു. എന്തുകൊണ്ടോ ആ കാലം ഗൃഹാതുരത്വം നിറഞ്ഞതായിരുന്നു. രാജീവൻ വലുതായാൽ ഓർക്കാൻ എന്താണുണ്ടാവുക? ഫ്ളാറ്റിനുള്ളിൽ അടച്ചുപൂട്ടിയ ഒരു ബാല്യം മാത്രം. ടിവി സീരിയലുകളുടെ വിരസത വീഡിയോഗെയ്മിൽ മുക്കി മാറ്റുന്നതു? പരീക്ഷകളിൽ റാങ്കു കിട്ടാനായി കളിസമയം വെട്ടിക്കുറക്കുന്നതു? അതിനിടയ്ക്കു് കൊല്ലത്തിലൊരിക്കലോ, അല്ലെങ്കിൽ വർഷങ്ങൾ കൂടുമ്പോഴോ ഇങ്ങിനെയൊരു യാത്രയിലുണ്ടാവുന്ന അനുഭവങ്ങൾ?
മോഹനനു് അവനോടു് സഹതാപം തോന്നി. ഇനി തിരിച്ചുപോയാൽ കുറച്ചു ദിവസത്തേയ്ക്കു് അവന്റെ ചോദ്യങ്ങൾക്കു് മറുപടി കൊടുക്കലാണു് തന്റെ പണി. ഇപ്പോൾ അവൻ ഒന്നും ചോദിക്കുന്നില്ല. ചോദ്യത്തിനുള്ള സമയംകൂടി പാഴാക്കരുതെന്നുണ്ടാവും അവന്നു്. ഇനി മദ്രാസിൽ തിരിച്ചുപോയാൽ അവന്റെ ചോദ്യങ്ങൾക്കു മറുപടി പറയാൻ അയാൾ വിഷമിക്കുന്നു, കാരണം അവൻ ചോദിക്കുന്നതെന്തിനെപ്പറ്റിയാണെന്നു് അയാൾക്കു് മനസ്സിലാവുന്നില്ല. ഭാഷ അവന്റെ പ്രശ്നമാണു്, പ്രത്യേകിച്ചും നാട്ടിൻപുറത്തെ കാര്യങ്ങളെപ്പറ്റി പറയുമ്പോൾ?
ഊണു് ഗംഭീരമായിരുന്നു. വീട്ടിൽ നട്ടുവളർത്തിയ പച്ചക്കറികളുടെ സ്വാദു് ആസ്വദിച്ചു് വല്ല്യമ്മ വിളമ്പിത്തന്ന ഊണുകഴിക്കുമ്പോൾ അയാൾ വീണ്ടും പറഞ്ഞു.
‘വല്ല്യമ്മ ഒരു കാര്യം ചെയ്യൂ. ഞങ്ങൾ ഇരുപത്തെട്ടാം തീയ്യതിയാണു് പോണതു്. ഞാൻ വല്ല്യമ്മയ്ക്കുകൂടി ടിക്കറ്റ് ബുക്ക് ചെയ്യാം. കുറച്ചുകാലം കൃഷ്ണൻകുട്ടീടെ ഒപ്പം പോയി താമസിക്കൂ.’
അവർ ഒന്നും പറഞ്ഞില്ല. അയാളുടെയും രാജീവന്റേയും ഊണു കഴിഞ്ഞശേഷം വല്ല്യമ്മയും ജാനകിയും ഊണുകഴിക്കാൻ തുടങ്ങി. ഒപ്പം ഊണു കഴിക്കാമെന്നയാൾ പറഞ്ഞുനോക്കിയതാണു്. അപ്പോൾ വിളമ്പൽ നടക്കില്ലെന്നു പറഞ്ഞു് അവർ ഒഴിഞ്ഞുമാറി. കൈകഴുകിവന്നു് അവർ ഊണു കഴിക്കുന്നതും നോക്കിയിരിക്കേ വല്ല്യമ്മ സംസാരിക്കാൻ തുടങ്ങി.
‘ഞാനീ വീട്ടിൽ വന്നിട്ടു് എത്ര കാലായിന്നറിയ്യോ? അറുപതു കൊല്ലം. പതിനഞ്ചാം വയസ്സിൽ കല്ല്യാണം കഴിച്ചു് വന്നതാണു്. ഇവിടെ എന്തൊക്കെ സംഭവിച്ചു. വല്ല്യച്ഛൻ മരിച്ചു. കല്യാണി മരിച്ചു. കൃഷ്ണൻകുട്ടീടെ അച്ഛൻ മരിച്ചു. കല്യാണിടെ കല്യാണം നടന്നതു് എനിക്കു് ഇപ്പഴും നല്ല ഓർമ്മണ്ടു്. കൃഷ്ണൻകുട്ടീനെ പ്രസവിച്ചതു് ഇവ്ട്ന്നാണു്. ഇപ്പോ എല്ലാം കഴിഞ്ഞു. എല്ലാം കഴിഞ്ഞ വീടാണിതു്. ഇവിടെ ഓർമ്മകള് എന്റെ ഒപ്പംണ്ടു്. മരിച്ചുപോയോരൊക്കെത്തന്നെ എന്റെ ഒപ്പംണ്ടു്. അവരെയൊക്കെ വിട്ടു് ഞാൻ വരണ്ടേ?’
കാര്യം ശരിയാണു്.
സന്ധ്യയുടെ അരങ്ങേറ്റം നടന്നു. ആകാശം ചുവന്നു, മരങ്ങൾ നിഴലുകളായി കാറ്റിൽ നേരിയ തണുപ്പിന്റെ കലർപ്പു്. ദൂരെനിന്നു് ഇടക്കയുടെ ശബ്ദം.
‘അമ്പലത്തിൽനിന്നാണു്. ദീപാരാധന.’
വല്ല്യമ്മ പറഞ്ഞു.
‘വല്ല്യമ്മ എന്നും അമ്പലത്തിൽ പോവാറുണ്ടല്ലോ. ഇന്നു് പോണില്ലേ?’
‘ഇപ്പോ എന്നും ഒന്നും പോവില്ല. കയറ്റല്ലേ. അതിനൊന്നും വയ്യ്യാണ്ടായിരിക്കുണു.’
വല്ല്യമ്മയുടെ നിർബ്ബന്ധത്തിനു വഴങ്ങി അയാൾ അന്നവിടെ താമസിക്കാമെന്നു് സമ്മതിച്ചു. പോകാത്തതു നന്നായിയെന്നയാൾക്കിപ്പോൾ തോന്നി. രാജീവനും ഇതു് ഓർമ്മിച്ചുവയ്ക്കാൻ പറ്റിയ ദിവസമായിരിക്കും.
വേലിക്കരികിൽ തലയുണങ്ങിയ ഒരു തെങ്ങ് അയാൾ കണ്ടു. അയാൾ വല്ല്യമ്മയോടു് ചോദിച്ചു.
‘എന്താണാ തെങ്ങ് ഉണങ്ങാൻ കാരണം?’
‘അതോ, അതു് മാറ്റിവച്ചതാ. എടവഴീക്കൂടെ വൈദ്യുതികമ്പി വലിക്കുമ്പോ അവരു് വെട്ടാൻ പറഞ്ഞു. ഞാന്നു് കെടക്ക്വായിരുന്നു. ഞാൻ മറ്റിക്കോളാംന്നു് പറഞ്ഞു. എങ്ങന്യാ പത്തമ്പതു വയസ്സുള്ള ഒരു കേടുംല്ല്യാത്ത തെങ്ങ് വെട്ടിക്കളയണതു? മാറ്റിവച്ചപ്പോ അതു് ഒണങ്ങും ചെയ്തു. ഒരു പ്രായൊക്കെ കഴിഞ്ഞാൽ മാറ്റിനടലൊന്നും ശര്യാവില്ല.’
അവർ എന്തൊക്കെയോ അർത്ഥംവച്ചു് പറയുകയാണെന്നു് മോഹനന്നു് തോന്നി. ശരിയായിരിക്കാം.
‘വല്ല്യമ്മ കണക്ഷനെടുത്തിട്ടില്ലേ?’
‘ഏയ്, അതിന്റെയൊക്കെ ആവശ്യം എന്താ. ഫാനിന്റെ ആവശ്യം ഇല്ല. രാത്രി നമ്മടെ പഴേ മേശവിളക്കു് കത്തിച്ചാൽ ധാരാളായി. രാത്രി പകലാക്കിയിട്ടു് എന്താ കാര്യം?’
ശരിയാണു്. ഇരുട്ടു് എന്താണെന്നറിയണമെങ്കിൽ നാട്ടിൻ പുറത്തുതന്നെ വരണം. രാത്രി കിടക്കുമ്പോൾ അയാൾ കൃഷ്ണൻകുട്ടിയെ ഓർത്തു. പന്ത്രണ്ടാം വയസ്സിൽ അമ്മ മരിച്ചു. ഇരുപത്തിരണ്ടാം വയസ്സിൽ അച്ഛനും. അമ്മമ്മയാണയാളെ വളർത്തിയെടുത്തതു്. ആ അമ്മമ്മ ഈ എഴുപത്തഞ്ചാം വയസ്സിൽ ഒറ്റയ്ക്കു താമസിക്കുന്നതു് അയാൾക്കു് വിഷമമുണ്ടാക്കുന്നുണ്ടു്. അയാൾ നേരിട്ടു സംസാരിക്കുമ്പോഴൊന്നും അവർ വഴങ്ങുന്നില്ല. അതുകൊണ്ടാണു് തന്നെ ദൂതനായി അയച്ചിരിക്കുന്നതു്. ഒരുപക്ഷേ, രാവിലെ ഒരിക്കൽക്കൂടി നിർബ്ബന്ധിച്ചാൽ അവർ സമ്മതിക്കുമായിരിക്കും. കൊച്ചുനാളിലെതൊട്ടു് മോഹനനു മനസ്സിലായിട്ടുള്ളതാണതു്. കാര്യമുള്ള സംഗതികളിലേ അയാൾ നിർബ്ബന്ധം പിടിക്കാറുള്ളു. അങ്ങിനെ നിർബ്ബന്ധം പിടിച്ച കാര്യങ്ങളെല്ലാംതന്നെ വല്ല്യമ്മ സമ്മതിച്ചുതന്നിട്ടുമുണ്ടു്. വല്ല്യമ്മയെ സംബന്ധിച്ചേടത്തോളം സ്വന്തം കല്യാണകാര്യമടക്കം കൃഷ്ണൻകുട്ടി തോറ്റിടത്തെല്ലാം അയാൾ ജയിക്കുകയാണുണ്ടായതു്. രാവിലെ ഒന്നുകൂടി പറഞ്ഞുനോക്കാം.
രാത്രി അയാൾ മുകളിൽ കൃഷ്ണൻകുട്ടിയുടെ മുറിയിൽ കിടന്നു. പണ്ടു് വേനലവധിക്കു് വരുമ്പോൾ ആ മുറിയിലാണു് കിടക്കാറു്. മറുവശത്തുള്ള കട്ടിലിൽ രാജീവൻ ഉറക്കമായി. അയാൾ എപ്പോഴാണു് ഉറങ്ങിയതെന്നോർമ്മയില്ല. ഉണർന്നപ്പോൾ നേരം നന്നായി വെളുത്തിരുന്നു. അയാൾ വാച്ചുനോക്കി. സമയം ആറരയായിട്ടേയുള്ളൂ. രാജീവൻ കുറച്ചുസമയംകൂടി കിടന്നോട്ടെ. അയാൾ കോണിയിറങ്ങി താഴേയ്ക്കു പോയി. വല്ല്യമ്മ ചായ കൊണ്ടുവന്നു. താൻ കോണിയിറങ്ങുന്ന ശബ്ദം അവർ കേട്ടുകാണും. അന്തരീക്ഷത്തിലെ പ്രസരിപ്പു് അയാൾ ശ്രദ്ധിച്ചു. ഇവിടെ നാട്ടിൻ പുറത്തു് ഓരോ പ്രഭാതവും പുതുമയോടെ എതിരേൽക്കുന്നു. നഗരത്തിന്റെ വിരസതയില്ല. വെറുതെ പുറത്തേയ്ക്കു നോക്കിയിരുന്നാൽ മതി. വൈചിത്ര്യം നിറഞ്ഞ കാഴ്ചകൾ. വല്ല്യമ്മയോടു് എന്താണു് പറയേണ്ടതെന്നു് അയാൾ തീർച്ചയാക്കിയിരുന്നില്ല. അതിനിടയ്ക്കു് അയാൾ മാറ്റിനട്ടു കരിഞ്ഞുപോയ തെങ്ങിനെ ഓർത്തു. അയാൾ ചോദിച്ചു.
‘ഞാൻ കൃഷ്ണൻകുട്ടിയോടു് എന്താണു് പറയേണ്ടതു?’
‘നീ ഒന്നും പറയണ്ട. വല്ലപ്പഴും ഒന്നു് വന്നു് കണ്ടാൽ മതി. ഇനി എനിക്കു് ഒരു പറിച്ചുനടൽ വയ്യ.’
രാജീവൻ താഴേയ്ക്കിറങ്ങി വന്നു.
‘നീയെന്താണു് കുടിക്കണതു? പാലു തരട്ടെ?’ വല്ല്യമ്മ ചോദിച്ചു.
‘വേണ്ട വല്ല്യമ്മേ’ മോഹനനാണു് പറഞ്ഞതു്, ‘അവൻ രാവിലെ ചായതന്നെയാണു് കുടിക്കാറു്.’
‘അച്ഛാ, നമ്മളെന്നാണു് പോണതു?’
‘ഇതാ, വല്ല്യമ്മ ദോശയുണ്ടാക്കിത്തന്നാൽ അതും കഴിച്ചു് ഇറങ്ങ്വാണു്.’
‘ഇത്ര വേഗോ?’ അവൻ നിരാശനായിയെന്നു തോന്നുന്നു. രണ്ടു ദിവസംകൂടി ഇവിടെ കൂടാമെന്നവൻ പ്രതീക്ഷിച്ചിരുന്നു. ഒരു പക്ഷേ, ഈ രണ്ടു ദിവസത്തേയ്ക്കായി അവൻ എന്തെങ്കിലും പ്ലാൻ ഉണ്ടാക്കിയിട്ടുമുണ്ടാവും. കയറാൻ കൂടുതൽ മരങ്ങൾ കണ്ടുവച്ചിട്ടുണ്ടാവും, തലേന്നു വൈകുന്നേരത്തെപ്പോലെ ആമ്പൽക്കുളത്തിൽ ഒന്നുരണ്ടു കുളികൂടി, ആ വലിയ പറമ്പിൽ ഇന്നലെ നിർത്തിവച്ച പര്യവേഷണത്തിന്റെ തുടർച്ച, അങ്ങിനെ പലതും.
വൈകുന്നേരത്തെ കുളി അവനു് നല്ലവണ്ണം ഇഷ്ടപ്പെട്ടിരുന്നു. ക്ലബ്ബിലെ നീന്തൽകുളത്തിലെ കുളിയിൽ നിന്നു് വ്യത്യസ്ഥമാണിതു്. ഇവിടെ പ്രകൃതിയും നിങ്ങളുടെ ഒപ്പമുണ്ടു്. വിരിഞ്ഞുനിൽക്കുന്ന ആമ്പൽപൂക്കൾ, കുളത്തിൽ അവിടവിടെയായി തഴച്ചു വളർന്ന ജലസസ്യങ്ങൾ, നഗ്നമായ ദേഹത്തു് പരൽമീനുകൾ വന്നു് കൊത്തുമ്പോഴുണ്ടാവുന്ന ഭയസമ്മിശ്രമായ കിക്കിളിയും, എല്ലാം ഉളവാക്കുന്ന അനുഭൂതി സ്വമ്മിങ്പൂളിൽ കിട്ടില്ല.
‘ഒരാഴ്ച ഇവിടെ താമസിച്ചിട്ടു് പതുക്കെ പോയാ മതി.’ വല്ല്യമ്മ പറഞ്ഞു.’നിന്റെ അച്ഛന്നു് അല്ലെങ്കിലും എപ്പഴും തിരക്കാ.’
അയാൾ അമ്മയെ ഓർത്തു. കൊല്ലത്തിൽ ഒരിക്കൽ നാട്ടിൽ വരുമ്പോൾ മകൻ അമ്മയുടെ അടുത്തുതന്നെ വേണമെന്നു് അവർക്കു് നിർബ്ബന്ധമാണു്. തന്റെ മനസ്സു വായിച്ചിട്ടെന്നപോലെ അവർ പറഞ്ഞു.
‘കാര്യം ശര്യാണു്. വല്ലപ്പഴും കാണുമ്പോ പാർവ്വതിക്കുംണ്ടാവില്ലെ മക്കളെ അടുത്തു് കിട്ടണംന്നു്.’
വല്ല്യമ്മയുടെ സ്വരത്തിൽ വേദനയുണ്ടോ?
കുന്നിറങ്ങുമ്പോൾ രാജീവിന്റെ മൂഡ് മോശമായിരുന്നു. അയാൾ ഒന്നും പറയാതെ നടന്നു. എന്തെങ്കിലും പറഞ്ഞാൽ അവൻ അതിന്മേൽ കയറിപിടിച്ചു് വീണ്ടും അവന്റെ മനസ്സു് കേടുവരുത്തും. തിരിഞ്ഞുനോക്കിയപ്പോൾ അയാൾ വല്ല്യമ്മ പടിക്കൽ നോക്കിനിൽക്കുന്നതു് കണ്ടു. കുറച്ചുമാറി പച്ചപ്പിൽ കുളിച്ചുനിൽക്കുന്ന പറമ്പിന്റെ ഒരറ്റത്തു് ഉണങ്ങിയ തെങ്ങും.

1943 ജൂലൈ 13-നു് പൊന്നാനിയിൽ ജനനം. അച്ഛൻ മഹാകവി ഇടശ്ശേരി ഗോവിന്ദൻ നായർ. അമ്മ ഇടക്കണ്ടി ജാനകി അമ്മ. കൽക്കത്തയിൽ വച്ചു് ബി. എ. പാസ്സായി. 1972-ൽ ലളിതയെ വിവാഹം ചെയ്തു. മകൻ അജയ് വിവാഹിതനാണു് (ഭാര്യ: ശുഭ). കൽക്കത്ത, ദില്ലി, ബോംബെ എന്നീ നഗരങ്ങളിൽ ജോലിയെടുത്തു. 1983-ൽ കേരളത്തിലേയ്ക്ക് തിരിച്ചു വന്നു. 1962 തൊട്ടു് ചെറുകഥകളെഴുതി തുടങ്ങി. ആദ്യത്തെ കഥാസമാഹാരം ‘കൂറകൾ’ 72-ൽ പ്രസിദ്ധപ്പെടുത്തി. പതിനഞ്ചു കഥാസമാഹാരങ്ങളും ഒമ്പതു് നോവലുകളും ഒരു അനുഭവക്കുറിപ്പും പ്രസിദ്ധപ്പെടുത്തിയിട്ടുണ്ടു്.
ആഡിയോ റിക്കാർഡിങ്, വെബ് ഡിസൈനിങ്, മൾട്ടിമീഡിയ പ്രൊഡക്ഷൻ, പുസ്തകപ്രസിദ്ധീകരണം എന്നിവയിൽ ഏർപ്പെട്ടിട്ടുണ്ടു്. 1998 മുതൽ 2004 വരെ കേരള സാഹിത്യ അക്കാദമി അംഗമായിരുന്നു.
